#pragma language glsl3

#ifdef VERTEX
uniform mat4 projection;
uniform mat4 transform;
uniform mat4 view;
vec4 position(mat4 transform_projection, vec4 vertex_position)
{    
    return  projection * view * transform * vertex_position;
}
#endif

#ifdef PIXEL
vec4 effect(vec4 color, Image tex, vec2 texture_coords, vec2 screen_coords)
{
    vec4 texturecolor = Texel(tex, texture_coords);
    return texturecolor * color;
}

#endif
