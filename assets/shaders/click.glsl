#pragma language glsl3

varying vec4 IDToFragment;

#ifdef VERTEX
attribute vec4 VertexTranslation;
attribute vec4 EntityID;
uniform mat4 projection;
uniform mat4 transform;
uniform mat4 view;
vec4 position(mat4 transform_projection, vec4 vertex_position)
{
    IDToFragment = EntityID;
    mat4 translate = mat4(vec4(1.0f,0.0f,0.0f,0.0f),
                          vec4(0.0f,1.0f,0.0f,0.0f),
                          vec4(0.0f,0.0f,1.0f,0.0f),
                          VertexTranslation);

    return  projection * view * transform * translate * vertex_position;
}
#endif

#ifdef PIXEL
vec4 effect(vec4 color, Image tex, vec2 texture_coords, vec2 screen_coords)
{
    vec4 c = IDToFragment;
    return  c;
}
#endif
