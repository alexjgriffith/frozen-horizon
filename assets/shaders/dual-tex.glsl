#pragma language glsl3

varying vec2 cTexVar;
varying vec2 nTexVar;
varying vec2 mTexVar;

#ifdef VERTEX
attribute vec2 cTexCoord;
attribute vec2 nTexCoord;
attribute vec2 mTexCoord;
attribute vec4 VertexTranslation;
uniform mat4 projection;
uniform mat4 transform;
uniform mat4 view;
vec4 position(mat4 transform_projection, vec4 vertex_position)
{
    mat4 translate = mat4(vec4(1.0f,0.0f,0.0f,0.0f),
                          vec4(0.0f,1.0f,0.0f,0.0f),
                          vec4(0.0f,0.0f,1.0f,0.0f),
                          VertexTranslation);
    cTexVar = cTexCoord;
    nTexVar = nTexCoord;
    mTexVar = mTexCoord;
    return  projection * view * transform * translate * vertex_position;
}
#endif

#ifdef PIXEL
uniform sampler2D cTex;
uniform sampler2D nTex;

vec4 effect(vec4 color, Image tex, vec2 texture_coords, vec2 screen_coords)
{
    vec4 texturecolor = Texel(cTex,  cTexVar);
    vec4 numcolor = Texel(nTex,   nTexVar);
    vec4 maskcolor = Texel(cTex, mTexVar);
    float a = numcolor.a * texturecolor.a;
    float an = 1 - numcolor.a;
    // if (numcolor.a == 0){
    //   return texturecolor;
    // }
    // else{
    //   return texturecolor * an + numcolor;
    // }
    vec4 t = texturecolor - maskcolor;
    vec4 c = vec4(t.r * an + numcolor.r,
                  t.g * an + numcolor.g ,
                  t.b * an + numcolor.b,
                  max(t.a,numcolor.a)
                  );
    return c;
}

#endif
