(local events {})

(tset events :list
      {:find-nothing {:type :message :description "Your party found nothing"}
       :nothing {:type :message :description "Your party found nothing"}
       :frolic {:type :message :description "Your party frolicked"}
       :nothing-overnight {:type :message :description "Your party slept through the night soundly"}
       :fail-to-find-inn {:type :message :description "Your party did not find an inn."}
       :fail-to-make-camp {:type :message :description "Your party got distracted and didn't make camp"}
       
       :find-normal-treasure {:type :get-items :description "Your party found some treasure"
                              :level-increase 0 :items [1 6] :types :all}
       :find-good-treasure {:type :get-items :description "Your party found some sweet treasure"
                            :level-increase 4 :items [1 4] :types :all}
       :library {:type :get-items :description "Your party found a book with information"
                 :level-increase 4 :items [1 1] :types :quest}
       :armoury {:type :get-items :description "Your party found some armour and weapons"
                 :level-increase 2 :items [1 4] :types :equipable}
       :delve {:type :get-items :description "Your party found some armour and weapons"
               :level-increase 2 :items [1 4] :types :trophy}
       :find-potions {:type :get-items :description "Your party found some potions"
                      :level-increase 2 :items [2 3] :types :potion}
       :directions {:type :get-items :description "Your party found some directions"
                    :level-increase 2 :items [1 1] :types :map}
       :explore-wreckage {:type :get-items :description "Your party found some directions"
                          :level-increase 2 :items [1 2] :types :all}
       :hide-from-duke {:type :get-items :description "Your party hid from the Frozen Duke"
                      :level-increase 2 :items [2 4] :types :missinformation}

       
       :attacked-by-normal {:type :combat :description "Your party was attacked"
                            :level-increase 0 :enemies [2 4]}
       :attacked-by-strong {:type :combat :description "Your party was attacked"
                            :level-increase 4 :enemies [1 2]}

       :no-action {:type :rest :description "Your party rested all day. They regained some health."
                   :health-bonus 0.3 :defense-camp-bonus 0}
       :make-camp {:type :rest :description "Your party made camp. They regained some health and set up defenses."
                   :health-bonus 0.2 :defense-camp-bonus 3}
       :find-inn {:type :rest :description "Your party found a nice inn to relax in. They regained some health and barricaded the door."
                  :health-bonus 0.5 :defense-camp-bonus 3}
       :listen-to-music {:type :rest :description "Your party listened to music. They are fully rested."
                         :health-bonus 1 :defense-bonus 0}

       ;; quests
       :solve-badger-problem {:type :finish-quest :description "Your party had a fruitful discussion with the badger. He paid you for your time."
                              :item :badger-art :finish :success :gold 200}
       :fail-badger-problem {:type :finish-quest :description "Your party destroyed a historic artifact. You were charged for the damages."
                             :finish :failure :gold -50}

       :solve-needs-quest {:type :finish-quest :description "Your party rescued the Mayor of Subzero from the Frozen Duke's henchmen. They Mayor shares some useful information about fabric that could be used for a sail."
                           :finish :success :gold 400 :item :fortress-quest}
       
       :solve-airship-quest {:type :finish-quest :description "Your party was successful in pulling off the icon. They found a bit of gold as well."
                             :item :ship-icon :finish :success :gold 100}
       :fail-airship-quest {:type :finish-quest :description "Your party hurt themselves trying to dislodge the icon. They all got injured"
                            :finish :failure :damage 20}

       :solve-touch-quest {:type :finish-quest :description "Your party knit some wonderful shoes. The mice where very appreciative and they gave you their mouse god statue and some gold for your trouble."
                             :item :mouse-statue :finish :success :gold 100}
       :fail-touch-quest {:type :finish-quest :description "Your party insulted the mice with their attempt. The local magistrate awarded them damages."
                          :finish :failure :gold -20}
       
       :solve-tower-quest {:type :finish-quest :description "Your party had a meaningful chat with the Fireflies. The gave you a piece of melted wall and some gold for your time"
                             :item :melted-wall :finish :success :gold 400}
       :fail-tower-quest {:type :finish-quest :description "You royally pissed off the Fireflies with your antics. They tried to melt you too. They all got injured."
                            :finish :failure :damage 40}

       :solve-hills-quest {:type :finish-quest :description "Your party carved some stairs into the Slippery Hill. The locals carved an ice statue of your party as a thanks, and threw in some gold."
                             :item :party-statue :finish :success :gold 400}
       :fail-hills-quest {:type :finish-quest :description "It was an absolute failure. Your party spent the whole day sliding down the hill. They all got injured"
                            :finish :failure :damage 40}


       :solve-hideout-quest {:type :finish-quest :description "Your party rummaged around the house snuggled int the mountain and found a dreadful painting of the Frozen Duke and some gold."
                             :item :duke-painting :finish :success :gold 400}
       :fail-hideout-quest {:type :finish-quest :description "Your party tried to make it up the mountain, but falling boulders stopped them. They all got injured."
                            :finish :failure :damage 40}
       
       :solve-frosthaven-quest {:type :finish-quest :description "They discover the source of their disaffection are the numerous crosses being horded by the Church of Our Frozen Grace."
                             :item :church-quest :finish :success :gold 600}

       :solve-colderado-quest {:type :finish-quest :description "They discover the source of the wails is coming from the far off Mage's Lookout."
                               :item :mage-quest :finish :success :gold 600}

       :solve-mage-quest {:type :finish-quest :description "Your party finds out Ol' Tom's been drinking lightening fluid. They take away his source and some gold for their trouble. He will come down eventually."
                               :item :lightener :finish :success :gold 1000}

       :solve-church-quest {:type :finish-quest :description "Your party offers to take an extra cross of their hands. The parishioners are so happy they shower you in gold."
                            :item :mast :finish :success :gold 1000}
       
       :solve-fortress-quest {:type :finish-quest :description "Your party sneaks through the Dukes Haunt stealing flags and knitting them into a sail. They also find a pile of gold."
                               :item  :sail :finish :success :gold 1000}
       
       }
      )

(fn events.message [e]
  (local messages (require :messages))
  (messages.write e.description))

(fn events.finish-quest [e hex location-details]
  (local {: quests : info} (require :state))
  (local {: handle} location-details)
  (local item (require :item))
  (local quest (. quests handle))
  (local party (require :party))
  (when quest
    (tset quest :finished e.finish)
    (when e.item
      (lume.clear hex.items)
      (local i (item.new e.item :location))
      (when i (table.insert hex.items i)))
    (when e.gold
      (tset info :gold (math.max 0 (+ info.gold e.gold))))
    (when e.damage
      (party.damage-members e.damage))
    )
  )

(fn events.get-items [e hex location-details]
  (local level location-details.level)
  (local item (require :item))
  (local is [])
  (local count (math.random (. e.items 1) (. e.items 2)))
  (local preferences
         (match e.types
           :all {:weapon 1 :armour 1 :potion 1 :map 1
                 :quest 1 :missinformation 1 :trophy 1 :part 1}
           :equipable {:weapon 1 :armour 1}
           :potion {:potion 1}
           :trophy {:trophy 1}
           :map {:map 1}
           :missinformation {:missinformation 1}
           :quest {:quest 1}))
  (lume.clear hex.items)
  (for [i 1 count]
    (local picked (item.pick level preferences :location))
    (when picked (table.insert hex.items picked)))
  )

(fn events.combat [e party location-details action-description]
  (local combat (require :combat-logic))
  (local {: handle : level} location-details)
  (local {: enemies : level-increase} e)
  (local count (math.random (. enemies 1) (. enemies 2)))
  (combat.fight count (+ level level-increase) handle e.description action-description)
  (local messages (require :messages))
  (messages.write e.description)
  (love.event.push :combat)
  )

(fn events.rest [e party]
  (local gs (party.get-members))
  (each [i g (ipairs gs)]
    (tset g :defense-camp-bonus (or e.defense-camp-bonus 0))
    (tset g :health (math.min g.max-health (+ g.health (* g.max-health (or e.health-bonus 0))))))
  )

(fn events.call [name tile-index action-description]
  (local locations (require :locations))
  (local {: hexes} (require :state))
  (local party (require :party))
  (local hex (. hexes tile-index))
  (local location-details (locations.get-details (or (?. hex :location) :wilderness)))
  (local e (. events.list name))
  (assert e (.. "Missing event " name))
  ;; Figure out how to gracefully handle missing events without crashing the game
  (if e
      (do (match e.type
            :get-items (events.get-items e hex location-details)
            :message (events.message e)
            :combat (events.combat e party location-details action-description)
            :rest (events.rest e party)
            :finish-quest (events.finish-quest e hex location-details))
          e)
      false))

events
