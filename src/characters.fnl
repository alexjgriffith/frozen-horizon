(local characters {})
(tset characters :characters
      {:mage-1    {:x 1 :y 1 :w 1 :h 1 :ox 0 :oy 0}
       :mage-2    {:x 2 :y 1 :w 1 :h 1 :ox 0 :oy 0}
       :mage-3    {:x 3 :y 1 :w 1 :h 1 :ox 0 :oy 0}
       :warrior-1 {:x 1 :y 2 :w 1 :h 1 :ox 0 :oy 0}
       :warrior-2 {:x 2 :y 2 :w 1 :h 1 :ox 0 :oy 0}
       :warrior-3 {:x 3 :y 2 :w 1 :h 1 :ox 0 :oy 0}
       :archer-1  {:x 1 :y 3 :w 1 :h 1 :ox 0 :oy 0}
       :archer-2  {:x 2 :y 3 :w 1 :h 1 :ox 0 :oy 0}
       :archer-3  {:x 3 :y 3 :w 1 :h 1 :ox 0 :oy 0}       
       })

(let [(iw ih w h) (values (assets.sprites.characters:getWidth)
                        (assets.sprites.characters:getHeight)
                        64
                        128)]
  (each [index character (pairs characters.characters)]
    (tset character :quad
          (lg.newQuad (* w (- character.x 1))
                      (* h (- character.y 1))
                      (* w character.w)
                      (* h character.h)
                      iw
                      ih
                      ))))

(fn characters.draw [which x y]
       (local character (. characters.characters which))
       (lg.draw assets.sprites.characters character.quad
                (+ (or x 0) character.ox ) (+ (or y 0) character.oy)))

characters
