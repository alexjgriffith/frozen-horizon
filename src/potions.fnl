{ ;; potions
 :health-potion-1
 {
  :name "Small Health Potion"
  :description "This potion will restore a small amount of health on use."
  :icon :potion
  :type :potion
  :colour colours.red
  :rarity :common
  :level 1
  :params {:target :health :value [30 50]}
  }

 :health-potion-2
 {
  :name "Health Potion"
  :description "This potion will restore a moderate amount of health on use."
  :icon :potion
  :type :potion
  :colour colours.red
  :rarity :common
  :level 4
  :params {:target :health :value [50 60]}
  }

  :health-potion-3
 {
  :name "Large Health Potion"
  :description "This potion will fully restore any gnome on use."
  :icon :potion
  :type :potion
  :colour colours.red
  :rarity :common
  :level 7
  :params {:target :health :value [200 300]}
  }
  
 :fight-potion-1
 {
  :name "Small Invigorater"
  :description "This potion will boost the fight value for a few days on use."
  :icon :potion
  :type :potion
  :colour colours.red
  :rarity :common
  :level 1
  :params {:target :potion-fight-value-bonus :value [1 2] :days [2 3]}
  }

  :fight-potion-2
 {
  :name "Invigorater"
  :description "This potion will slightly boost the fight value for several days on use."
  :icon :potion
  :type :potion
  :colour colours.red
  :rarity :common
  :level 4
  :params {:target :potion-fight-value-bonus :value [2 3] :days [3 5]}
  }


  :fight-potion-3
 {
  :name "Large Invigorater"
  :description "This potion will max the fight value for many days on use."
  :icon :potion
  :type :potion
  :colour colours.red
  :rarity :common
  :level 7
  :params {:target :potion-fight-value-bonus :value [5 6] :days [7 15]}
  }


  :defense-potion
 {
  :name "Mixture of Stone"
  :description "This potion will increase your fight value for many days on use."
  :icon :potion
  :type :potion
  :colour colours.red
  :rarity :common
  :level 4
  :params {:target :potion-defense-bonus :value [5 6] :days [7 15]}
  }

 }
