(local tiles (require :tiles))
(local party (require :party))
(local interface (require :interface))
(local item (require :item))
(local fade (require :lib.fade))
(local flux (require :lib.flux))

(tiles.start)
(party.start tiles.tavern 8 48)

(local windv {:volume 0.05})

(flux.to windv 10 {:volume 0})

(fn enter [gs]
  (fade.in)
  (interface.tavern.next-day)
  (assets.sounds.page:play)
  (assets.music.fire:stop)
  ;; (item.new :rusty-short-sword :party)
  ;; (item.new :padded-jacket :party)
  ;; (item.new :health-potion-1 :party)
  ;; (item.new :town-map-1 :party)
  (item.new :airship-quest :party)
  ;; (item.new :town-1-quest :party)
  ;; (item.new :village-quest :party)
  ;; (item.new :hills-quest :party)
  ;; (interface.tavern.toggle)
  (sounds.fire:setVolume 0.1)
  (sounds.wind:setVolume windv.volume)
  (sounds.birds:stop)
  ;;(assets.sounds.wind:stop)
  (sounds.bgm:play)
  (interface.close-all)
  )

(var (ox oy) (values -16 0))
(set ox (- ox window.fso))
(set oy (- oy window.fsy))
;; (local canvas (lg.newCanvas window.w window.h))


(fn draw [gs]
  ;;(lg.setCanvas canvas)
  (lg.origin)
  (lg.push :all)
  (local (w h) (love.window.getMode))
  (local (mx my) (love.mouse.getPosition))
  ;; (local (mx my) (values (+ mx 70)))
  (lg.translate (- ox) (- oy))
  
  (lg.clear colours.shadow)
  (tiles.draw-ground ox oy)
  (tiles.draw-nav ox oy)
  (tiles.draw-cover ox oy)
  (if ui.map-edit
      (tiles.draw-grid ox oy)
      (tiles.draw-hover ox oy))
  
  (tiles.draw-icons ox oy)
  
  (party.draw)
  (lg.pop)
  (lg.push)
  (lg.translate window.fso window.fsy)
  (lg.setFont text-font)
  (lg.setColor colours.white)
  (local {:info {: easy-mode}} (require :state))  
  (when easy-mode
    (lg.printf "Casual Mode Enabled" 0 (- 700 40) (- 900 20) :right))
    
  (local hover-hex (require :hover-hex))
  (hover-hex.draw)
  
  (each [_ i (ipairs interface.list)]
    (when (and i.active i.draw)
      (i.draw mx my)))

  (local hover-item (require :hover-item))
  (hover-item.draw)
  
  (local messages (require :messages))
  (messages.draw)
  
  (item.draw-hand (- mx window.fso) (- my window.fsy))
  
  (lg.pop)
  (lg.push :all)
  (lg.pop)

  (lg.setColor colours.dark-green)
  ;;(lg.rectangle :fill window.fso 0 ox 1000)
  (lg.rectangle :fill window.fso -10000 -10000 20000)
  (lg.rectangle :fill (+ 1000 window.fso) -10000 10000 20000)
  (lg.rectangle :fill window.fso (+ window.fsy 700) 1000 10000)
  (lg.rectangle :fill window.fso window.fsy 1000 -10000)
  (lg.setColor 1 1 0.8 0.1)
  (love.graphics.setBlendMode "subtract")
  (local sdepth 6)
  (lg.rectangle :fill window.fso (+ window.fsy sdepth) (- sdepth) 700 )
  (lg.rectangle :fill window.fso (+ window.fsy 700) (- 1000 sdepth) sdepth)
  (love.graphics.setBlendMode "alpha")
    (lg.setColor 1 1 1 1)
  (lg.draw fade.canvas)
  (gifcat:capture)

  )

(fn update [gs dt]
  (local (mx my) (love.mouse.getPosition))
  (tiles.update dt mx my ox oy)
  (party.update dt)
  (item.update dt)
  (local hover-hex (require :hover-hex))
  (hover-hex.update dt)
  (local hover-item (require :hover-item))
  (hover-item.update dt)
  )

(fn mousepressed [gs mx my button]
  (tiles.mousepressed mx my button ox oy)
  )

(fn keypressed [gs key keycode]
  (match key
    :p (love.graphics.captureScreenshot :screenshot.png)
    _ (tiles.keypressed key keycode)))


{: update : draw : keypressed : mousepressed : keypressed : enter}
