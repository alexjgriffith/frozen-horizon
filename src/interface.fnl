(local icons (require :icons))
(local {: info : items} (require :state))
(local item (require :item))

(local interface {})

(fn set-color-if-in [what? mx my w h colour-true colour-false]
  (local (what args)
         (match (type what?)
           :table (values (. what? 1) (. what? 2))
           _ (values what? nil)
           ))
  (local {: hover : items} (require :state))
  (local (screen-mx screen-my) (lg.inverseTransformPoint mx my))
  (local hover (and ;; (not hover.click-down)
                    (not items.hand)
                    (_G.pointWithin screen-mx screen-my 0 0 w h)))
  (lg.setColor (if hover colour-true colour-false))
  (when hover (love.event.push :hover what args))
  )

(fn dragging [what mx my w h colour-true colour-false]
  (local {: hover : items } (require :state))
  (local (screen-mx screen-my) (lg.inverseTransformPoint mx my))
  (local hover (and hover.click-down
                    items.hand
                    (_G.pointWithin screen-mx screen-my 0 0 w h)))
  (lg.setColor (if hover colour-true colour-false))
  (when hover (love.event.push :hover what))
  )

(fn draw-inventory-box [mx my x y where index]
  (local item-id (item.get-id where index))
  (local (bw bh) (values 40 40))
  (when item-id
    (lg.push)
    (lg.translate -4 -4)
    (item.draw item-id 0 0)
    (lg.pop))
  (local hand items.hand)
  (local item-hover
         (and item-id
              (set-color-if-in
               [:item item-id] mx my bw bh colours.black colours.brown)))

  (local slot-hover (and ;; (not item-id)
                         (dragging
                          :item mx my bw bh colours.black colours.brown)))
  
  (when (and item-hover (love.mouse.isDown 1))
    (db (love.keyboard.isDown :lctrl) :click+ctrl)
    (match [where (love.keyboard.isDown :lctrl)]
      [_ false] (love.event.push :click :item [item-id :hand])
      [:inventory true] (love.event.push :click :item [item-id :gnome])
      [:gnome true] (love.event.push :click :item [item-id :inventory])))
  
  (when slot-hover
    (love.event.push :item-in-hand-at where index))
  
  (if (or item-hover slot-hover)
      (lg.setColor colours.black))
  (lg.rectangle :line 0 0 bw bh)
  )

(fn draw-consumable-box [mx my w h where index]
  (local item-id (item.get-id where index))
  (local slot-hover  (dragging
                      :item mx my w h colours.black [1 1 1 0]))
  (when slot-hover
    (lg.setColor colours.black)
    (love.event.push :item-in-hand-at where index))
  (lg.rectangle :line 0 0 w h)
  )


(fn check-click [hover event-name ...]
  (when (and hover (love.mouse.isDown 1))
    (love.event.push :click event-name ...)))

(fn draw-button [mx my w h text oy font tag args]
  (lg.push :all)
  (local hover (set-color-if-in
               tag mx my w h colours.black colours.brown))
  (check-click hover tag args)
  (lg.rectangle :line 0 0 w h)
  (lg.setFont font)
  (lg.printf text -40 oy (+ w 80) :center)
  (lg.pop)
  hover
  )

(fn draw-gnome-box [mx my id hover]
  (local {: gnomes } (require :state))
  (local gnome (. gnomes.list id))
  (if hover
      (lg.setColor colours.black)
      (lg.setColor colours.brown))
  (lg.setFont text-font)
  (lg.print (.. gnome.name " - "gnome.class) 4)
  (lg.setFont small-text-font)
  (lg.print (.. "L:" gnome.level
                " A:" gnome.attack
                (if (> (+ gnome.potion-attack-bonus gnome.attack-bonus) 0)
                    (.. "+" (+ gnome.potion-attack-bonus gnome.attack-bonus))
                    "")
                " D:" gnome.defense
                (if (> (+ gnome.defense-bonus gnome.potion-defense-bonus) 0)
                  (.. "+" (+ gnome.defense-bonus gnome.potion-defense-bonus))
                  "")
                " F:" gnome.fight-value
                (if (> (+ gnome.fight-value-bonus gnome.potion-fight-value-bonus) 0)
                  (.. "+" (+ gnome.fight-value-bonus gnome.potion-fight-value-bonus))
                  "")
                ) 4 20)
  (lg.print (.. "HP " gnome.health "/" gnome.max-health) 4 32)
  )

(fn draw-gnome [mx my id w h party-id]
  (local {: gnomes } (require :state))
  (local gnome (. gnomes.list id))
  (lg.rectangle :line (- w 96) 0 96 h)
  (local hover (draw-button mx my (- w 106) h "" 0 big-button-font
                             :focus-gnome-party party-id))
  (draw-consumable-box mx my (- w 106) h (.. :p party-id) 1)
  (lg.push)
  (lg.translate 160 5)
  (draw-inventory-box mx my 0 0 (.. :e party-id) 1)
  (lg.pop)
  (lg.setColor colours.brown)
  (lg.push)
  (lg.translate 205 5)
  (draw-inventory-box mx my 0 0 (.. :e party-id) 2)
  (lg.pop)
  (draw-gnome-box mx my id hover)
  )

(fn draw-party [mx my]
  (local {: party } (require :state))
  (local (w h b) (values 250 50 12))
  (lg.push)
  (each [i id (ipairs party.members)]
    (lg.setColor colours.brown)
    (draw-gnome mx my id w h i)
    (lg.translate 0 (+ h b))

    (when (= (% i 3) 0)
      (lg.translate (+ w b) (- (* 3 (+ h b))))
      )
    )
  (lg.pop)
  )


(fn draw-focused-party-gnome [mx my party-id]
  (local {: party : gnomes} (require :state))
  (local gnome (require :gnome))
  (local targeted-gnome (. party :members party-id))
  (local (w h b) (values 250 50 12))
  (lg.push)
  (lg.translate (- 510 30 10) 10)
  (draw-button mx my 30 30 "x" -5 big-button-font :unfocus-gnome-party)
  (lg.pop)
  (lg.setLineWidth 6)
  (lg.rectangle :line 0 0 510 170)
  (lg.setLineWidth 1)
  (gnome.draw-sprite-by-id-no-info targeted-gnome 20 15)
  (lg.setColor colours.brown)
  (lg.push)
  (lg.translate 140 30)
  (draw-gnome mx my targeted-gnome w h party-id)
  (lg.translate 0 60)
  (draw-button mx my 250 50 "Fire Gnome" 5 big-button-font :fire-gnome party-id)
  (local g (. gnomes.list targeted-gnome))
  (lg.pop))

(fn draw-location-inventory [mx my tile-index]
  (local (bw bh w h b) (values
                        40 40
                        (+ (* 40 4) (* 12 5)) (+ (* 40 6) (* 12 7)) 12))
  ;; (lg.translate (- b) b)
  (lg.push)
  (lg.setColor colours.brown)
  (lg.rectangle :line 0 0 (+ 83 (* 8 bw) (* 9 b)) (+ bh (* 2 b)))
  (lg.translate b b)
  (lg.setFont button-font)
  (lg.print "Location" 0 0)
  (lg.print "Items" 0 20)
  (lg.translate 83 0)
  (for [i 1 8]
    (lg.setColor colours.brown)
    (draw-inventory-box mx my 0 0 :location i)
    (lg.translate (+ bw b) 0))
  (lg.pop)
  )

(fn draw-tavern-inventory [mx my]
  (local (bw bh w h b) (values
                        40 40
                        (+ (* 40 4) (* 12 5)) (+ (* 40 6) (* 12 7)) 12))
  ;; (lg.translate (- b) b)
  (lg.push)
  (lg.setColor colours.brown)
  (lg.rectangle :line 0 0 (+ 83 (* 8 bw) (* 9 b)) (+ bh (* 2 b)))
  (lg.translate b b)
  (lg.setFont button-font)
  (lg.print "Tavern" 0 0)
  (lg.print "Wall" 0 20)
  (lg.translate 83 0)
  (for [i 1 8]
    (lg.setColor colours.brown)
    (draw-inventory-box mx my 0 0 :tavern i)
    (lg.translate (+ bw b) 0))
  (lg.pop)
  )


(fn draw-gnome-inventory [mx my]
  (local (bw bh w h b) (values
                        40 40
                        (+ (* 40 4) (* 12 5)) (+ (* 40 6) (* 12 7)) 12))
  ;; (lg.translate (- b) b)
  (lg.push)
  (lg.setColor colours.brown)
  (lg.rectangle :line 0 0 (+ 60 (* 6 bw) (* 7 b)) (+ bh (* 2 b)))
  (lg.translate b b)
  (lg.setFont button-font)
  (lg.print "For" 0 0)
  (lg.print "Sale" 0 20)
  (lg.translate 60 0)
  (for [i 1 6]
    (lg.setColor colours.brown)
    (draw-inventory-box mx my 0 0 :gnome i)
    (lg.translate (+ bw b) 0))
  (lg.pop)
  )


(fn draw-inventory [mx my]
  (lg.push)
  (local (bw bh w h b) (values
                        40 40
                        (+ (* 40 4) (* 12 5)) (+ (* 40 6) (* 12 7)) 12))
  (lg.setColor colours.brown)
  (lg.rectangle :line 0 0 w h)
  (lg.translate b b)
  (for [i 1 24]
    (draw-inventory-box mx my 0 0 :party i)
    (lg.setColor colours.brown)
    (lg.translate (+ bw b) 0)
    (when (= (% i 4) 0)
      (lg.translate (- (* 4 (+ bw b))) (+ bh b))
      )
    )
  (lg.translate (- b) b)
  (lg.setColor colours.brown)
  (lg.rectangle :line 0 0 (+ (* 4 bw) (* 5 b)) (+ bh (* 2 b)))
  (lg.translate b b)
  (lg.print :Use 0 5)
  (lg.translate 35 0)
  (draw-inventory-box mx my 0 0 :use 1)
  (lg.setColor colours.brown)
  (lg.translate 50 0)
  (lg.print :Destroy 0 5)
  (lg.translate 65 0)
  (draw-inventory-box mx my 0 0 :trash 1)
  (lg.pop)
  )

(fn draw-tavern-gnome-focus [mx my targeted-gnome]
  (local gnome (require :gnome))
  (local {: gnomes} (require :state))
  (local party (require :party))
  (if targeted-gnome
      (do
        (local g (. gnomes.list targeted-gnome))
        (gnome.draw-sprite-by-id targeted-gnome)
          (lg.push)
          (lg.translate 100 40)
          (draw-gnome-box mx my targeted-gnome)
          (lg.push)
          (lg.translate 140 15)
          (lg.setFont button-font)
          (lg.print (.. "Cost: " g.cost) 0 0)
          (lg.pop)
          (lg.translate 284 0)
          (when (party.at-home)
            (draw-button mx my 100 50 "Hire" 5 big-button-font
                         :hire targeted-gnome))
          (lg.pop)
          (lg.push)
          (lg.translate 100 100)
          (let [{: items} (require :state)]
            (tset items.inventories :gnome g.items)
            )
          (draw-gnome-inventory mx my)
          (lg.pop)
        )
      (lg.printf "Select a Gnome above" 0 0 500 :center)
      ))

(fn draw-tavern-gnomes [mx my w]
  (local {: gnomes} (require :state))
  (local gnome (require :gnome))
  (lg.push)
  (local (w2 h n) (values 64 128 (# gnomes.tavern)))
  (local b (math.floor (/ (- w (* w2 n)) (- n 1))))
  (each [i g (ipairs gnomes.tavern)]
    (lg.push :all)
    (gnome.draw-sprite-by-id g)
    (let [hover (set-color-if-in
                 :gnome mx my 64 178 colours.black [0 0 0 0])]
      (lg.rectangle :line 0 0 64 178)
      (check-click hover :targeted-gnome g))    
    (lg.pop)
    (lg.translate (+ w2 b) 0)
    )
  (lg.pop)
  (lg.translate 0 (+ h 70))
  )

(local tavern {:id :tavern :active false :targeted-gnome false
               :defaults {:active false :targeted-gnome false}})
(fn tavern.open []
  (interface.close-all)
  (sounds.fire:play)
  (assets.sounds.page:stop)
  (assets.sounds.page:play)
  (tset tavern :targeted-gnome false)
  (tset tavern :active true))

(fn tavern.close []
  (sounds.fire:stop)
  (assets.sounds.page:stop)
  (assets.sounds.page:play)
  (tset tavern :active false))

(fn tavern.toggle []
  (if tavern.active
       (tavern.close)
       (tavern.open)))

(fn tavern.next-day []
  (local {: gnomes} (require :state))
  (local gnome (require :gnome))  
  (tset gnomes :tavern [])
  (local {: items} (require :state))
  (var target-level 1)
  (each [_ trophy-id (ipairs items.inventories.tavern)]
    (local trophy (. items.list trophy-id))
    (when trophy.params.target-level
      (set target-level (+ target-level trophy.params.target-level)))
    )
  ;; (local target-level (math.random 1 10))
  (local new-gnomes (math.random 4 5))
  ;; (local new-gnomes 6)
  (set tavern.targeted-gnome nil)
  (for [i 1 new-gnomes]
    (table.insert gnomes.tavern (gnome.new-random target-level)))
  )

(fn tavern.focus-on-gnome [g]
  (tset tavern :targeted-gnome g)
  )

(fn tavern.draw [mx my]
  (lg.push :all)
  (local (width height) (values 800 650))
  (lg.translate (/ (- 900 width) 2) (/ (- 700 height) 2))
  (lg.setColor colours.white)
  (lg.rectangle :fill 0 0 width height)
  (lg.setColor colours.brown)
  (lg.setLineWidth 6)
  (lg.rectangle :line 0 0 width height)
  (lg.setLineWidth 1)
  (lg.translate 0 0)
  (do
    (lg.setFont title-font)
    (lg.printf "Friggin Cold Tavern" 0 0  width :center))
  ;; x button
  (let [(x y w h) (values (- width 10 30) 10 30 30)]
    (lg.push)    
    (lg.translate x y)
    (local hover (set-color-if-in :x mx my w h colours.black colours.brown))
    (when (and hover (love.mouse.isDown 1))
        (love.event.push :click :x :tavern))
    (lg.rectangle :line  0 0 w h)
    (lg.setFont subtitle-font)
    (lg.print "x" 5 -14)
    (lg.pop))
  (lg.setColor  colours.brown)
  (lg.translate 0 60)
  (do (lg.setFont subtitle-font)
      (lg.printf "Tavern" 0 0 width :center))  
  (lg.translate 0 60)
  ;;hline
  (lg.line 20 0 780 0)
  (lg.push)
  (lg.translate 20 20)
  (draw-tavern-inventory mx my)
  (lg.pop)
  (lg.translate 0 20)
  (lg.setFont button-font)
  ;; (lg.printf (..  tile-details.description) 20 0 (- height 40) :left)
  (do
    (lg.push)
    (lg.translate 550 20)
    (local party (require :party))
    (if (party.at-home)
        (do (lg.printf "Party is In" 0 0 250 :center))
        (do (lg.printf "Party is Out" 0 0 250 :center))
        )
    (lg.pop)
    )
  (local (ew eh eb) (values 250 50 12))
  (lg.translate 0 80)
  ;;intentory
  (lg.push)
  (lg.translate 550 0)
  (lg.line 0 5 0 320)
  (lg.translate 15 0)
  (draw-inventory mx my)
  (lg.pop)
  (lg.push)
  (lg.translate 20 0)
  (draw-tavern-gnomes mx my 500)
  (lg.line 20 0 480 0)
  (lg.translate 0 20)
  (draw-tavern-gnome-focus mx my tavern.targeted-gnome) 
  (lg.pop)
  
  (lg.pop)
  )


(local tile-details {})
(tset tile-details :defaults
      {:id :tile-details
       :active false
       :name "The Wilderness"
       :type "wilderness"
       :description "Your party is out in the wilderness. The wolves are howling in the distance. It's cold."
       :actions []})
(each [key value (pairs tile-details.defaults)]
  (tset tile-details key value))

(fn tile-details.open [tile-index force?]
  (assets.sounds.page:stop)
  (assets.sounds.page:play)
  (interface.close-all)
  (local locations (require :locations))
  (local {: hexes} (require :state))
  (local poi (?.  hexes tile-index :location))
  (local location-name (or poi :wilderness))
  (local {: quests} (require :state))
  (local quest (. quests location-name))
  (tset tile-details :tile-index tile-index)

  (local {: type : icon : name : information : actions}
         (locations.get-details location-name))
  (local {: items : hexes} (require :state))
  (tset items.inventories :location (. hexes tile-index :items))
  
  (tset tile-details :type type)
  (tset tile-details :description information)
  (tset tile-details :name name)
  (when quest (pp quest))
  (tset tile-details :actions (if (and quest (or (not quest.finished) (not (= quest.finished :success))))
                                  (let [c (lume.clone actions)]
                                    (table.insert c quest.action)
                                    c)
                                  actions))
      
  (when (or poi force?)
    (tset tile-details :active true))
  )

(fn tile-details.close []
  (assets.sounds.page:stop)
  (assets.sounds.page:play)
  (each [key value (pairs tile-details.defaults)]
    (tset tile-details key value))
  (tset tile-details :active false))

(fn tile-details.toggle [tile-index ...]
  (if tile-details.active
      (tile-details.close)
        (tile-details.open tile-index ...)))

(fn tile-details.update [dt mx my button])
(fn tile-details.draw [mx my]
  (lg.push :all)
  (lg.translate 50 25)
  (local height 800)
  (local width 650)
  (lg.setColor colours.white)
  (lg.rectangle :fill 0 0 height width)
  (lg.setColor colours.brown)
  (lg.setLineWidth 6)
  (lg.rectangle :line 0 0 height width)
  (lg.setLineWidth 1)
  (lg.translate 0 0)
  (do
    (lg.setFont title-font)
    (lg.printf tile-details.name 0 0  height :center))
  ;; x button
  (let [(x y w h) (values (- height 10 30) 10 30 30)]
    (lg.push)    
    (lg.translate x y)
    (local hover (set-color-if-in :x mx my w h colours.black colours.brown))
    (when (and hover (love.mouse.isDown 1))
        (love.event.push :click :x :tile-details))
    (lg.rectangle :line  0 0 w h)
    (lg.setFont subtitle-font)
    (lg.print "x" 5 -14)
    (lg.pop))
  (lg.setColor  colours.brown)
  (lg.translate 0 60)
  (do (lg.setFont subtitle-font)
      (lg.printf "Party Location" 0 0 height :center))
  (lg.translate 0 60)
  ;;hline
  (lg.line 20 0 780 0)
  (lg.translate 0 20)
  (lg.setFont button-font)
  (lg.printf (..  (or tile-details.description "MISSING DESCription")) 20 0 (- height 40) :left)
  (local (ew eh eb) (values 250 50 12))
  (lg.translate 0 80)
  ;;inventory
  (lg.push)
  (lg.translate 550 0)
  (lg.line 0 5 0 320)
  (lg.translate 15 0)
  (draw-inventory mx my)
  (lg.pop)
  ;; actions
  (lg.push)
  (local {: active-event} (require :state))
  (local party (require :party))
  (if (party.empty)
      (do
        :nothing)
   (not active-event.action)
      (do ;; draw possible actions
        (lg.translate 20 (- (+ eh eb)))
        (local actions (require :actions))
        (each [i action-id (ipairs tile-details.actions)]
          (local action (. actions.list action-id))
          (lg.translate 0 (+ eh eb))
          (when (< i 5)
            (when (= (% i 3) 0)
              (lg.translate (+ ew eb) (- (* 2 (+ eh eb))))
              )
            (let [hover (set-color-if-in :e1 mx my ew eh colours.black colours.brown)]
              (when (and hover (love.mouse.isDown 1))
                (love.event.push :click :action [action tile-details.tile-index]))
              (lg.rectangle :line 0 0 ew eh)
              (lg.setFont button-font)
              (if (?. action :name)
                  (lg.printf (. action.name 1) 0 10 ew :center)
                  (lg.printf "ERROR:MISSING-ACTION-NAME" 0 10 ew :center)
                  )
              (lg.setFont text-font)
              ;; (lg.printf "Lvl:1 Type:Combat" 0 22 ew :center)
              )))
        )
      (do ;; draw action outcome
        (lg.translate 20 0)
        (lg.setFont button-font)
        (lg.printf (.. "Action: " active-event.action.description) 0 0 500 :left)
        (lg.translate 0 50)
        (lg.printf (.. "Outcome: " active-event.event.description) 0 0 500 :left)
        )
      )
  (lg.pop)
  (lg.translate 0 (+ (* eh 2) (* eb 1) 19))
  (lg.setColor colours.brown)
  ;;hline
  (if (party.empty)
      (do
        (lg.push)
        (lg.translate 120 0)
        (draw-button mx my 300 50 "Hire Gnome" 5 big-button-font :tavern nil)
        (lg.pop)
        )
      (lg.line 25 0 (+ (* ew 2) (* 2 eb)) 0))
  (lg.translate 0 19)
  (lg.push)
  (lg.translate 20 0)
  (local {: hover} (require :state))
  (if hover.focused-gnome-party-index
      (draw-focused-party-gnome mx my hover.focused-gnome-party-index)
      (draw-party mx my)
      )
  (lg.translate 0 (+ (* eh 3) (* eb 2) 12))
  (draw-location-inventory mx my)
  (lg.pop)
  (lg.pop)
  )


(local side-bar {:id :side-bar :active true :defaults {:active true}})

(fn side-bar.draw [mx my]
  (local party (require :party))
  (lg.push)
  (lg.setColor colours.white)
  (lg.rectangle :fill 900 0 100 800)
  (lg.setColor colours.brown)
  (lg.setLineWidth 6)
  (lg.line 897 0 897 800)
  (lg.setLineWidth 1)

  (lg.push)
  (let [(ndx ndy ndw ndh) (values 905 620 90 75)]
    (local {: wiggles} (require :state))
    (lg.translate ndx ndy)
    (lg.push)
    (lg.translate (/ ndw 2) (/ ndh 2))
    (lg.rotate wiggles.next-day)
    (lg.translate (- (/ ndw 2)) (- (/ ndh 2)))
    (when (not info.next-day-lock)
      (let [hover (set-color-if-in :next-day mx my ndw ndh
                                 colours.black colours.brown)]
      (when (and hover (love.mouse.isDown 1))
        (love.event.push :click :next-day)
        )
      (lg.rectangle :line 0 0 ndw ndh)
      (lg.rectangle :fill 0 0 ndw ndh)
      (lg.setColor colours.white)
      (lg.rectangle :line 2 2 (- ndw 4) (- ndh 4))
      (lg.setFont big-button-font)
      (lg.printf "Next" 0 5 ndw :center)
      (lg.printf "Day" 0 (+  30 5) ndw :center)))
    (lg.pop))
  (let [(ndx ndy ndw ndh) (values 0 -60 90 50)]
    (lg.translate ndx ndy)
    (let [hover (set-color-if-in :party mx my ndw ndh
                                 colours.black colours.brown)]
      (when (and hover (love.mouse.isDown 1))
        (love.event.push :click :party))
      (lg.rectangle :line 0 0 ndw ndh)
      (lg.setFont button-font)
      (lg.printf "Party" 0 12 ndw :center)
      ))

  (let [(ndx ndy ndw ndh) (values 0 -60 90 50)]
    (lg.translate ndx ndy)
    (let [hover (set-color-if-in :tavern mx my ndw ndh
                                 colours.black colours.brown)]
      (when (and hover (love.mouse.isDown 1))
        (love.event.push :click :tavern))
      (lg.rectangle :line 0 0 ndw ndh)
      (lg.setFont button-font)
      (lg.printf "Tavern" 0 12 ndw :center)
      ))

    (let [(ndx ndy ndw ndh) (values 0 -60 90 50)]
    (lg.translate ndx ndy)
    (let [hover (set-color-if-in :log mx my ndw ndh
                                 colours.black colours.brown)]
      (when (and hover (love.mouse.isDown 1))
        (love.event.push :click :log))
      (lg.rectangle :line 0 0 ndw ndh)
      (lg.setFont button-font)
      (lg.printf "Log" 0 12 ndw :center)
      ))
    
    (let [(ndx ndy ndw ndh) (values 0 -60 90 50)]
      (lg.translate ndx ndy)
      (let [hover (set-color-if-in :menu mx my ndw ndh
                                   colours.black colours.brown)]
        (when (and hover (love.mouse.isDown 1))
          (love.event.push :click :menu))
      (lg.rectangle :line 0 0 ndw ndh)
      (lg.setFont button-font)
      (lg.printf "Menu" 0 12 ndw :center)
      ))

    (let [(ndx ndy ndw ndh) (values 0 -260 90 250)]
      (lg.translate ndx ndy)
      (let [hover (set-color-if-in :duke mx my ndw ndh
                                   colours.black colours.brown)]
        (when (and hover (love.mouse.isDown 1))
          (love.event.push :click :duke))
        (lg.rectangle :line 0 0 ndw ndh)
        (lg.rectangle :fill 43 25 9 200)
        (lg.setColor 1 1 1 1)
        (icons.draw (if (< info.duke 0)
                        :eye-closed
                        :eye-open
                        )
                    0 (-> (- 200 info.duke) (math.max 0) (math.min 200)))
        ;; (lg.setColor colours.red)
        ;; (lg.circle :fill 45 25 20)
        ;; (lg.circle :line 45 25 20)
      
      ))
    
    
  (lg.pop)
  (lg.setColor 1 1 1)
  (icons.draw :fortress 900 50)
  (lg.setColor colours.brown)
  (lg.setFont button-font)
  ;;(lg.printf party.state 900 0 90 :center)
  (lg.printf (.. "Day: " info.day) 902 0 90 :left)
  (lg.printf (.. "Gold: " info.gold) 902 25 90 :left)
  (lg.pop)
)

(local menu {:id :menu :active false :defaults {:active false}})
(fn menu.close []
  (tset menu :active false))

(fn menu.open []
  (assets.sounds.page:stop)
  (assets.sounds.page:play)
  (interface.close-all)
  (tset menu :active true))

(fn menu.close []
  (assets.sounds.page:stop)
  (assets.sounds.page:play)
  (tset menu :active false))

(fn menu.toggle []
  (if  menu.active
    (menu.close)
    (menu.open)))

(fn menu.draw [mx my]
  (lg.push :all)
  (let [(x y w h) (values 300 100 400 500)]
    (lg.translate x y)
    (lg.setColor colours.white)
    (lg.rectangle :fill 0 0 w h)
    (lg.setColor colours.brown)
    (lg.setLineWidth 6)
    (lg.rectangle :line 0 0 w h)
    (lg.setLineWidth 1)
    (lg.setFont subtitle-font)
    (lg.printf "Menu" 0 0 w :center)
    (lg.setFont big-button-font)
    (lg.printf "Frozen Horizons" 0 40 w :center)
    (lg.line 20 80 (- w 20) 80)
    
  (let [(x y w h) (values (- 400 10 30) 10 30 30)]
    (lg.push)    
    (lg.translate x y)
    (local hover (set-color-if-in :x mx my w h colours.black colours.brown))
    (when (and hover (love.mouse.isDown 1))
        (love.event.push :click :x :menu))
    (lg.rectangle :line  0 0 w h)
    (lg.setFont subtitle-font)
    (lg.print "x" 5 -14)
    (lg.pop))

  (lg.translate 30 120)
  (let [(ew eh eb) (values 340 50 15)]
    (let [hover (set-color-if-in :e1 mx my ew eh
                                 colours.black colours.brown)]
      (when (and hover (love.mouse.isDown 1))
        (love.event.push :click :x :menu))
      (lg.rectangle :line 0 0 ew eh)
      (lg.setFont big-button-font)
      (lg.printf "Continue" 0 8 ew :center))
    
    (lg.translate 0 (+ eh eb))
    (let [hover (set-color-if-in :e1 mx my ew eh
                                 colours.black colours.brown)]
      (when (and hover (love.mouse.isDown 1))
        (love.event.push :click :reset))
      (lg.rectangle :line 0 0 ew eh)
      (lg.setFont big-button-font)
      (lg.printf "Reset" 0 8 ew :center))

    (lg.translate 0 (+ eh eb))
    (let [hover (set-color-if-in :e1 mx my ew eh
                                 colours.black colours.brown)]
      (when (and hover (love.mouse.isDown 1))
        (love.event.push :click :credits))
      (lg.rectangle :line 0 0 ew eh)
      (lg.setFont big-button-font)
      (lg.printf "Credits" 0 8 ew :center))

    (lg.translate 0 (+ eh eb))
    (let [hover (set-color-if-in :e1 mx my ew eh
                                 colours.black colours.brown)]
      (when (and hover (love.mouse.isDown 1))
        (love.event.push :click :mute))
      (lg.rectangle :line 0 0 ew eh)
      (lg.setFont big-button-font)
      (lg.printf (if (ui.get-mute) "Unmute" "Mute") 0 8 ew :center))

    (lg.translate 0 (+ eh eb))
    (let [hover (set-color-if-in :e1 mx my ew eh
                                 colours.black colours.brown)]
      (when (and hover (love.mouse.isDown 1))
        (love.event.push :click :quit))
      (lg.rectangle :line 0 0 ew eh)
      (lg.setFont big-button-font)
      (lg.printf "Quit" 0 8 ew :center))
    
       ))
  (lg.pop)
  )

(local credits
       {:id :credits :active false :defaults {:active false}
        :info
        [["Game Code and Art" "AlexjGriffith" "https://gitlab.com/alexjgriffith/frozen-horizon" "GPL3+ / CCBY 4.0" "Yes"]
         ["Winter (Music)" "Cyber SDF" "https://soundcloud.com/cybersdf/winter" "CCBY" "No"]
         ["Page (Sound Effect)" "Nicole Marie T" "https://opengameart.org/content/page-turning-sfx-sound-effect" "CCBY 4.0" "No"]
         ["Chime (Sound Effects)" "PWL" "https://opengameart.org/content/bell-dingschimes" "CCO" "No"]
         ["Variety of Curors" "Kemono" "https://opengameart.org/content/variety-of-cursors" "CCBY 3.0" "No"]
         ]
        })
(fn credits.open []
  (assets.sounds.page:stop)
  (assets.sounds.page:play)
  (interface.close-all)
  (tset credits :active true))
(fn credits.close []
  (assets.sounds.page:stop)
  (assets.sounds.page:play)
  (tset credits :active false))
(fn credits.draw [mx my]
  (lg.push :all)
  (let [(x y w h) (values 50 50 800 600)]
    (lg.translate x y)
    (lg.setColor colours.white)
    (lg.rectangle :fill 0 0 w h)
    (lg.setColor colours.brown)
    (lg.setLineWidth 6)
    (lg.rectangle :line 0 0 w h)
    (lg.setLineWidth 1)
    (lg.setFont subtitle-font)
    (lg.printf "Credits" 0 0 w :center)
    (lg.setFont big-button-font)
    (lg.printf "Frozen Horizons" 0 40 w :center)
    (lg.line 20 80 (- w 20) 80)
    
  (let [(x y w h) (values (- 800 10 30) 10 30 30)]
    (lg.push)    
    (lg.translate x y)
    (local hover (set-color-if-in :x mx my w h colours.black colours.brown))
    (when (and hover (love.mouse.isDown 1))
        (love.event.push :click :x :credits))
    (lg.rectangle :line  0 0 w h)
    (lg.setFont subtitle-font)
    (lg.print "x" 5 -14)
    (lg.pop)
    )

  (let [(ew eh eb) (values 370 108 15)]
    (lg.translate 30 (- 100 eh eb))
    (each [i info (ipairs credits.info)]
      (lg.translate 0 (+ eh eb))
      (when (= (% i 5) 0)
        (lg.translate 380 (- (* 4 (+ eh eb))))
        )
      (let [hover (set-color-if-in (.. :link i) mx my ew eh colours.black colours.brown)]
        (when (and hover (love.mouse.isDown 1))
          (love.event.push :click :link (. info 3)))
      (lg.rectangle :line 0 0 ew eh)
      (lg.setFont mono-font)
      (lg.printf (.. "Title:    " (. info 1)
                     "\nAuthor:   " (. info 2)
                     "\nLicence:  " (. info 4)
                     "\nModified: " (. info 5)
                     "\nLink: \n" (string.sub (. info 3) 1 100))
                 4 2 (- ew 8) :left))
    )
    )
  (lg.pop)
  ))

(local popup {:id :popup :active true
              :item-id nil
              :hex-id nil
              :defaults {:active false :item-id nil}})
(fn popup.close []
  (tset popup :active false)
  )

(fn popup.open [item-id hex-id]
  ;; (interface.close-all)
  (tset popup :item-id item-id)
  (tset popup :hex-id hex-id)
  (tset popup :active true))

(fn popup.toggle [item-id hex-id]
  (if popup.active
      (popup.open item-id hex-id)
      (popup.close)))

(fn popup.draw [mx my]
  (local (w h) (values 600 110))
  (local (x y) (values (/ (- 900 w) 2) 10))
  (if popup.item-id
      (do
        (local {: items} (require :state))
        (local item (require :item))
        (local item-id popup.item-id)
        (local i (. items.list item-id))
        (when i
          (local {: name : description : cost : sell : handle
                  : params : type : rarity : level : current-location} i)
          (lg.push :all)
          (lg.translate x y)
          (lg.setColor colours.white)
          (lg.rectangle :fill 0 0 w h)
          (lg.setColor colours.brown)
          (lg.setLineWidth 6)
          (lg.rectangle :line 0 0 w h)
          (lg.setLineWidth 1)
          (item.draw item-id 0 8)
          (lg.setFont big-button-font)
          (lg.setColor colours.brown)
          (lg.printf name 0 4 w :center)
          (lg.setFont button-font)    
          (local r (match rarity :one :unique :common :common :legendary :legenday _ ""))
          (assert r (.. "Item " handle " is missing rarity."))
          (lg.printf (.. r " " type) 0 30 w :center)
          (match current-location
            :party (lg.printf (.. sell " Gold") 0 30 (- w 10) :right)
            :gnome (lg.printf (.. cost " Gold") 0 30 (- w 10) :right))
          (lg.push)
          (lg.translate 10 55)
          (lg.setFont button-font)
          (lg.printf description 0 0 (- w 20) :left)
          (lg.pop)
          (lg.pop))
        )
      )
  )


(local log {:id :log :active false :defaults {:active false}})
(fn log.close []
  (tset log :active false))

(fn log.open []
  (assets.sounds.page:stop)
  (assets.sounds.page:play)
  (interface.close-all)
  (tset log :active true))

(fn log.close []
  (assets.sounds.page:stop)
  (assets.sounds.page:play)
  (tset log :active false))

(fn log.toggle []
  (if  log.active
    (log.close)
    (log.open)))

(fn log.draw [mx my]
  (lg.push :all)
  (let [(x y w h) (values 150 100 600 500)]
    (lg.translate x y)
    (lg.setColor colours.white)
    (lg.rectangle :fill 0 0 w h)
    (lg.setColor colours.brown)
    (lg.setLineWidth 6)
    (lg.rectangle :line 0 0 w h)
    (lg.setLineWidth 1)
    (lg.setFont subtitle-font)
    (lg.printf "Combat Log" 0 0 w :center)
    
    (let [(x y w h) (values (- w 10 30) 10 30 30)]
      (lg.push)    
      (lg.translate x y)
      (local hover (set-color-if-in :x mx my w h colours.black colours.brown))
      (when (and hover (love.mouse.isDown 1))
        (love.event.push :click :x :log))
      (lg.rectangle :line  0 0 w h)
      (lg.setFont subtitle-font)
      (lg.print "x" 5 -14)
      (lg.pop))
    (local {: combat} (require :state))
    (lg.setFont button-font)
    (lg.line 20 60 (- w 40) 60 )
    (lg.printf combat.action-description 20 65 (- w 40) :center)

    (lg.printf combat.event-description 20 95 (- w 40) :center)
    (lg.push)
    (lg.translate 20 135)

    (lg.setFont subtitle-font)
    (each [wo column (ipairs [combat.outcome-party combat.outcome-monsters])]
      (lg.push)
      (each [i p (ipairs column)]
        (when (~= p.name "")
          (when p.dead
            (lg.push)
            (lg.translate 100 0)
            (lg.rotate 0.25)
            (lg.translate -100 0)
            (lg.translate 40 3)
            (local [r g b] colours.red)
            (lg.setColor r g b 0.5)
            (lg.print "DEAD")
            (lg.pop)
            )
          (draw-button mx my 180 50 p.name 5 big-button-font
                       :combat-log-result [(match wo
                                             1 :party
                                             2 :monsters) i])
          (lg.setColor colours.brown)
          (lg.setFont button-font)
          (match wo
            1 (do :party)
            2 (do :monsters
                  (lg.printf (.. :L p.level) 0 0 175 :right)
                  ))
          (lg.setFont subtitle-font)
          (lg.setColor colours.red)
          (lg.push)
          (lg.translate 185 0)
          (when (~= 0 p.damage)
            (lg.print (.. "-" p.damage) ))
          (lg.pop))
        (lg.translate 0 60)
        )
      (lg.pop)
      (lg.translate 310 0)
      )
    (lg.pop)
    (lg.push)
    (lg.setColor colours.brown)
    (lg.translate 290 130)
    (lg.line 0 0 0 350)
    (lg.pop)
    ;; (lg.push)
    ;; (lg.translate (+ 300 20) 80)
    
    ;; (draw-button mx my 160 50 :Name 5 big-button-font :combat-log-result [:party 1])
    ;; (lg.pop)

  )
  (lg.pop)
  )


(local dukeinfo {:id :dukeinfo :active false :defaults {:active false}})
(fn dukeinfo.close []
  (tset dukeinfo :active false))

(fn dukeinfo.open []
  (assets.sounds.page:stop)
  (assets.sounds.page:play)
  (interface.close-all)
  (tset dukeinfo :active true))

(fn dukeinfo.close []
  (assets.sounds.page:stop)
  (assets.sounds.page:play)
  (tset dukeinfo :active false))

(fn dukeinfo.toggle []
  (if  dukeinfo.active
    (dukeinfo.close)
    (dukeinfo.open)))

(fn dukeinfo.draw [mx my]
  (lg.push :all)
  (let [(x y w h) (values 150 50 600 600)]
    (lg.translate x y)
    (lg.setColor colours.white)
    (lg.rectangle :fill 0 0 w h)
    (lg.setColor colours.brown)
    (lg.setLineWidth 6)
    (lg.rectangle :line 0 0 w h)
    (lg.setLineWidth 1)
    (lg.setFont subtitle-font)
    (lg.printf "Quests and Information" 0 0 w :center)
    
    (let [(x y w h) (values (- w 10 30) 10 30 30)]
      (lg.push)    
      (lg.translate x y)
      (local hover (set-color-if-in :x mx my w h colours.black colours.brown))
      (when (and hover (love.mouse.isDown 1))
        (love.event.push :click :x :dukeinfo))
      (lg.rectangle :line  0 0 w h)
      (lg.setFont subtitle-font)
      (lg.print "x" 5 -14)
      (lg.pop))
    (local {: info} (require :state))
    (lg.setFont button-font)
    (lg.line 20 60 (- w 40) 60 )


    (lg.translate 0 65)
    ;; (lg.translate 0 10)
    ;; (lg.setFont button-font)
    ;; (lg.printf "Frozen Duke Status: " 20 0 (- w 20) :left)
    ;; (lg.translate 0 25)
    ;; (lg.line 20 0 (- w 400) 0 )
    ;; (lg.translate 0 5)

    (local verbage (if (< info.duke 0) "The Frozen Duke has not yet noticed you."
                       (< info.duke 100) "The Frozen Duke has begun his hunt. Spread misinformation to avoid his gaze."
                       (< info.duke 150) "The Frozen Duke is close to finding you. Spread misinformation to avoid his gaze."
                       (< info.duke 200) "The Frozen Duke has almost found you. Spread misinformation to avoid his gaze."
                       ))
    (lg.printf verbage 20 0 (- w 40) :left)
    (lg.push)
    (lg.translate 0 40)

    
    (local  {: items} (require :state))
    (local item-map {})
    (each [_ item-id (ipairs items.inventories.party)]
      (local item (. items.list item-id))
      (tset item-map item.handle true))

    (lg.translate 0 10)
    ;; (lg.line 20 0 (- w 40) 0 )
    (lg.translate 0 10)
    (lg.setFont button-font)
    (lg.printf "Ship Part Checklist: " 20 0 (- w 20) :left)
    (lg.translate 0 25)
    (lg.line 20 0 (- w 400) 0 )
    (lg.translate 0 5)
    (lg.setFont button-font)
    
    (lg.printf (.. "Sail " (if (. item-map :sail) "found" "needed")) 20 0 (- w 20) :left)
    (lg.translate 0 20)
    (lg.printf (.. "Mast " (if (. item-map :mast) "found" "needed")) 20 0 (- w 20) :left)
    (lg.translate 0 20)
    (lg.printf (.. "Lightener " (if (. item-map :lightener) "found" "needed")) 20 0 (- w 20) :left)
    (lg.translate 0 20)

    (lg.translate 0 10)
    (lg.setFont button-font)
    (lg.printf "Active Quests: " 20 0 (- w 20) :left)
    (lg.translate 0 25)
    (lg.line 20 0 (- w 400) 0 )
    (lg.translate 0 5)
    ;;(when (and (. item-map :sail) (. item-map :mast) (. item-map :lightener))
    (local locations (require :locations))
    (local actions (require :actions))
    (local {: quests} (require :state))
    (each [location {: action : finished} (pairs quests)]
      (local quest-name (?. actions :list action :name 1))
      ;; (pp quest-name)
      ;; (pp location)
      (local location-name (. locations :list location :name))
      (when (and quest-name (or (not finished) (~= finished :success)))
        (lg.printf (.. location-name " - " quest-name) 20 0 (- w 20) :left)
        (lg.translate 0 20)
        )
      
      )
    
    (lg.pop)

    (lg.translate 100 450)
    (draw-button mx my 400 50 "Toggle Casual Mode" 4 big-button-font :toggle-casual-mode)

    
    

    
  )
  (lg.pop)
  (lg.push :all)
  (lg.pop)
  )


(tset interface :list [menu tile-details tavern credits log dukeinfo popup side-bar])
(tset interface :menu menu)
(tset interface :tile-details tile-details)
(tset interface :credits credits)
(tset interface :tavern tavern)
(tset interface :popup popup)
(tset interface :log log)
(tset interface :dukeinfo dukeinfo)

(fn interface.ui-active []
  (var ui-active false)
  (each [_ i (ipairs interface.list)]
    (when (and i.active (not (= :side-bar i.id)))
      (set ui-active true)))
  ui-active)

(fn interface.close-all []
  (each [_ i (ipairs interface.list)]
    (when (and i.active (not (= :side-bar i.id)))
      (i.close))))

interface
