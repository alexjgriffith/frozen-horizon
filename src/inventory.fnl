;; put inventory things here
(local state (require :state))

(fn new-highest-empty [map len]
  (var highest-empty false)
  (for [i 1 len]
    (when (and (. map i) (not highest-empty))
      (set highest-empty i)))
  highest-empty)

(fn add-item [to to-index item-name]
  "Insert item-name into inventory array to at to-index. 
If to-index is null use the highest-empty.
Return :no-space-remains if no space remains"
  (local inventories state.items.inventories)
  (local inventory (. inventories to))
  (local highest-empty  (. inventory  :highest-empty))
  (local target (or to-index highest-empty))
  (match target
    false (values false :no-space-remains)
    _
    (do
      ;; keep an array for rapid iteration
      (table.insert inventory.index to-index target)
      ;; members is the reverse map of map, used for determining
      ;; if an item type is in hand
      (if inventory.members
          (tset inventory.members item-name [target])
          (table.insert (. inventory.members item-name) target))
      ;; map holds the reference to the item name
      (tset inventory.map target item-name)
      (tset inventory :highest-empty
            (new-highest-empty inventory.map inventory.max-length))
      true))
  )

(fn remove-item [to to-index]
  (local inventories state.items.inventories)
  (local inventory (. inventories to))
  (local in-list (lume.any inventory.index (fn [i] (= i to-index))))
  (match in-list
    false (values false :no-index-in-inventory)
    _
    (do
      (table.remove inventory.index to-index)
      (local name (. inventory.map to-index))
      (tset inventory.map to-index nil)
      (match (type (. inventory.members name))
        :table (do (table.remove (. inventory.members name) to-index)
                  (when (= (# (. inventory.members name)) 0)
                    (tset inventory.members name nil))))
      (tset inventory :highest-empty
            (new-highest-empty inventory.map inventory.max-length))
      true
      )))

(fn has-item [from from-index]
  (local inventories state.items.inventories)
  (local inventory (. inventories from))
  (match (. inventory :map from-index )
    nil (values false :no-index-in-inventory)
    _ true))

(fn has-space [to]
  (local inventories state.items.inventories)
  (local inventory (. inventories to))
  (match (< (# inventory.index) inventory.max-length)
    true true
    flase (false :no-space-remains)))

(fn transfer-item [to to-index from from-index item-name]
  (local (has has-err) (has-item from from-index))
  (local (space space-err) (has-space to to-index))
  (match [has space]
    [false _] (values false has-err)
    [_ false] (values false space-err)
    [true true] (do (remove-item from from-index)
                    (add-item to to-index item-name))))

(fn item-seen [item-name types]
  (local inventories state.items.inventories)
  (lume.any inventories (fn [inv] (and (. types type)
                                       (. inv.members item-name)))))

(fn item-init [inventory-params]
  (local items {:list (lume.clone (require :items))
                :inventories {}
                :hand false
                :next-id 1
                :over false})
  (each [_ {: name : type : max-length} (ipairs inventory-params)]
    (tset items.inventories name (new-inventory name type max-length))))

(fn hand-grab [from from-index item-name]
  (tset state.items :hand {: from : from-index : item-name}))

(fn hand-put [to to-index]
  (local {:items.hand {: from : from-index : item-name}})
  (transfer-item to to-index from from-index item-name)
  (tset items.hand false))

(fn new-inventory [name type max-length]
  (local inventories state.items.inventories)
  (local inventory
         {:map {}
          :index []
          :members {}
          :highest-empty 1
          :max-length max-length
          :type type
          :name name})
  (tset inventories name inventory)
  inventory)


(fn item-details [item-name]
  (local i (. state.items.list item-name))
  (match i
    nil (values false :no-item)
    _ i))

(fn hand-details []
  state.items.hand)

(fn item-set-value [item-name key value]
  (local i (. state.items.list item-name))
  (match i
    nil (values false :no-item)
    _ (do
        (tset i key value)
        true)))

(fn fold-contents [to fun args col? all?]
  "Apply fun to all members of to passing in args and col?.
If all? is not nil then apply to all posible members of the 
inventory.
col = fun (inventory-index item args col)"
  (local inventory (. state.items.inventories to))
  (match inventory
    nil (values false :no-inventory)
    _ (do
        (var col (or col? {}))
        (if all?
            (for [i 1 inventory.max-length]
              (set col (fun i (. inventory.map i) args col)))
            (each [i n (ipairs inventory.index)]
              (set col (fun n (. inventory.map n) args col))))
        col
        )))

{: init-items
 : new-inventory
 : add-item
 : remove-item
 : transfer-item
 : item-seen
 : item-details
 : item-set-value
 : hand-details
 : fold-contents
 : hand-grab
 : hand-put
 }
