(local hex (require :hex))
(local icons (require :icons))
(local locations (require :locations))

(var {: hexes : party : info} (require :state))

(local width 12)
(local height 8)
(var map-name :map-2)
(var tavern 52)
(var fortress 66)
(var target tavern)
(var state :targeting)
(var path [])
(var ui-active false)

(fn load-map [map-name-in]
  (set map-name map-name-in)
  (local {: types : visible : locations} (require map-name))
  (for [i 0 width]
    (for [j 0 height]
      (local index (hex.ij-to-index i j height))
      (tset hexes index {:type (or (. types index) :water)
                         :visible (or (. visible index) false)
                         :location (or (. locations index) false)
                         :border [(unpack (hex.xy i j 42 48))]
                         :shape [(unpack (hex.xy i j 48 48))]
                         :bounds (hex.get-bounds i j 48)
                         :neighbours (hex.get-neighbours i j height)
                         :items []
                         :events []
                         :within false
                         :neighbour-within false
                         :neighbour-visible false
                         })
      (when (= (. locations index) :tavern)
        (set tavern index))
      (when (= (. locations index) :fortress)
        (set fortress index))
      )))



(fn save [save-name?]
  (local fennel (require :lib.fennel))
  (local data {:types {} :visible {} :locations {}})
  (each [index h (ipairs hexes)]
    (when (~= (. h :type) :water)
      (tset (. data :types) index (. h :type)))
    (when (~= (. h :visible) false)
      (tset (. data :visible) index (. h :visible)))
    (when (~= (. h :visible) false)
      (tset (. data :locations) index (. h :location))))
  (love.filesystem.write (or save-name? map-name) (fennel.view data))
  )

(load-map map-name)

(local tiles {})
(fn tiles.set-index-value [index key value]
  (tset (. hexes index) key value))


(fn tiles.update [dt mx my ox oy]
  ;; set within
  (each [index h (ipairs hexes)]
    (tset h :neighbour-within false)
    (tset h :neighbour-visible false))
  (local {: locations-visible} (require :state))
  (each [index h (ipairs hexes)]
    (when h.visible
      (tset locations-visible h.location true))
    (local within (hex.point-within
                   h.shape
                   (+ mx (or ox 0))
                   (+ my (or oy 0))))
    (tset h :within within)
    (when within
      (if (= h.type :water)
          (set target party.index)
          (set target index)
          )
      (each [_ n (ipairs h.neighbours)]
        (when (. hexes n)
          (tset (. hexes n) :neighbour-within true))))
    (when h.visible
      (each [_ n (ipairs h.neighbours)]
        (when (. hexes n)
          (tset (. hexes n) :neighbour-visible true))))
    )
  (local interface (require :interface))
  (set ui-active (interface.ui-active))
  (local party (require :party))
  (when (and (= state :targeting)
             (> (# party.members) 0)
             (not ui-active))
      (set path (tiles.path party.index target info.moves))
      )
  (when (= 0 (# party.members))
    (set state :targeting)
    (set path []))
  )

(fn tiles.party-distance-to-fortress []
  (local path (tiles.path (or party.index tavern) fortress 100000))
  (if path
      (- (# path) 1)
      0))

(fn tiles.draw-ground [ox oy]
  (lg.push :all)
  (lg.setColor 1 1 1 1)
  (lg.setLineWidth 1)
  (lg.setLineStyle :smooth)
  (each [index h (ipairs hexes)]
    (lg.setColor
     (match h.type
       :water colours.bright-blue
       :green colours.olive
       _ colours.grey))
    (lg.polygon :fill h.shape)
    (lg.polygon :line h.shape)
    )
  (lg.pop))

(fn tiles.draw-hover [ox oy]
  (local {: info} (require :state))
  (tset info :hex-location false)
  (each [index h (ipairs hexes)]
    (local visible (or h.visible h.neighbour-visible ui.map-edit))
    (when (and (not ui-active) visible h.within (~= h.type :water))
      (when h.visible (tset info :hex-location h.location))
      (lg.setColor colours.red)
          (lg.setLineWidth 4)
          (lg.polygon :line h.shape))))

(fn tiles.draw-grid [ox oy]
  (lg.push :all)
  (lg.setColor 1 1 1 1)
  (lg.setLineWidth 1)
  (lg.setLineStyle :smooth)
  (each [index h (ipairs hexes)]
    (if h.within
        (lg.setColor 1 0 0 1)
        (and false h.neighbour-within)
        (lg.setColor 1 1 0 1)
        (lg.setColor 0 0 0 0.2))
    (when (or (~= h.type :water) ui.map-edit)
      (lg.print (.. index "") (+ 26 (. h :bounds 1)) (+ 12 (. h :bounds 2)))
      (lg.polygon :line h.border))
    )
  (lg.pop))

(fn tiles.draw-nav [ox oy]
  (lg.push :all)
  (lg.setColor 1 1 1 1)
  (lg.setLineWidth 1)
  (lg.setLineStyle :smooth)
  (when (and path (> (# path ) 0))
    (local line [])
    (if (= state :targeting)
        (lg.setColor colours.yellow)
        (lg.setColor colours.shadow))
    (each [_ index (ipairs path)]
      (local (i j) (hex.index-to-ij index height))
      (local (midx midy) (hex.ij-to-xy i j 48))
      (lg.circle :fill midx (+ midy 16) 10)
      (lg.circle :line midx (+ midy 16) 10)
      (table.insert line midx )
      (table.insert line (+ midy 16))
      )
    (lg.setLineWidth 6)
    (lg.line line))
  (lg.pop))


(fn tiles.draw-icons [ox oy]
  (lg.push :all)
  (lg.setColor 1 1 1 1)
  (lg.setLineWidth 1)
  (lg.setLineStyle :smooth)
  (each [index h (ipairs hexes)]
    (local visible (or h.visible  ui.map-edit))
    (when (and h.location visible)
      (icons.draw (. locations.list h.location :icon)
                  (+ 0 (. h :bounds 1))
                  (+ 0 (. h :bounds 2))))
    )
  (lg.pop))

(local (ww wh) (love.window.getMode))
(local shadow-canvas (lg.newCanvas ww wh))

(fn tiles.draw-cover [ox oy]
  (lg.push :all)
  (lg.setColor 1 1 1 1)
  (lg.setLineWidth 1)
  (lg.setLineStyle :smooth)
  (lg.setCanvas shadow-canvas)
  (lg.clear 0 0 0 0)
  (lg.setBlendMode :lighten :premultiplied)
  (each [index h (ipairs hexes)]
    (local visible (or h.visible  ui.map-edit))
    (var nvisible h.neighbour-visible)
    (local [r g b] colours.shadow)
    (local a
           (match [visible nvisible]
             [true _] 0
             [_ true] 0.7
             [_ _] 1
             ))
    (lg.setColor r g b a)
    (lg.polygon :line h.shape)
    (lg.polygon :fill h.shape)

    )
  (lg.setCanvas)
  (lg.setBlendMode :alpha :alphamultiply)
  (lg.setColor 1 1 1 1)
  (lg.translate ox oy)
  (lg.draw shadow-canvas)
  (lg.pop)
  )

(fn stack [...]
  (local stack-fns {:get (fn [q] (let [ret (. q (# q))]
                                 (tset q (# q) nil)
                                 ret
                                 ))
                  :put (fn [q value] (table.insert q value))
                  :empty (fn [q] (= (# q) 0))
                  })
  (local stack-mt {:__index stack-fns})
  (setmetatable [...] stack-mt))

(fn queue [...]
  (local queue-fns {:get (fn [q] (when (<= q.index (# q.data))
                                   (local ret (. q.data q.index))
                                   (set q.index (+ q.index 1))
                                   ret
                                   ))
                    :put (fn [q value]
                           (table.insert q.data value))
                    :empty (fn [q] (< (# q.data) q.index))
                  })
  (local queue-mt {:__index queue-fns})
  (setmetatable {:data [...] :index 1} queue-mt))

(fn get-neighbors [index]
  (icollect [_ i (ipairs (. hexes index :neighbours))]
    (when (and (. hexes i)
               (~= (. hexes i :type) :water)
               ;; (or ui.map-edit
               ;;     (. hexes i :visible)
               ;;     (. hexes i :neighbour-visible)
               ;;     )
               )
      i)))

(fn nav [index1 index2 limit]
  ;; breadth first search
  (var front (queue index1))
  (local path {index1 :START})
  (while (not (front:empty))
    (local current (front:get))
    (each [_ next (ipairs (get-neighbors current))]
      (when (not (. path next))
        (front:put next)
        (tset path next current))
      ))
  (var ret [])
  (var previous index2)
  (while (~= previous :START)
    (table.insert ret previous)
    (set previous (. path previous))
    )
  (local len (# ret))
  (when (> len limit)
    (set ret (lume.slice ret (- len limit ) len))
    )
  ret
  )

(fn tiles.path [index1 index2 limit]
  (var path [])
  (if (= index1 index2)
      []
      (= limit 0)
      []
      (or (= (. hexes index2 :type) :water)
          (not (or ui.map-edit
                   (. hexes index2 :visible)
                   (. hexes index2 :neighbour-visible)
                   )))
      nil
      (nav index1 index2 limit)
      )
  )

(fn set-target []
  (when (and path (> (# path ) 0) (= state :targeting) (not ui-active))
      (local line []) 
      (each [_ index (ipairs path)]
        (local (i j) (hex.index-to-ij index height))
        (local (midx midy) (hex.ij-to-xy i j 48))
        (table.insert line [midx midy index i j]))
      (love.event.push :set-path line)
      (set state :waiting)
    ))

(fn get-info []
  (each [index value (ipairs hexes)]
    (when (and value.within value.location)
      (pp (. locations.list value.location))
      )))

(fn tiles.mousepressed [mx my button ox oy]
  (if ui.map-edit
    (each [index h (ipairs hexes)]
      (local within (hex.point-within h.shape (+ mx ox) (+ my oy)))
      (when within
        (match button
          1 (tiles.set-index-value index :type :ground)
          2 (tiles.set-index-value index :type :water)))
      )
    (match button
      1 (set-target)
      )
  ))

(fn tiles.start []
  (load-map map-name)
  )

(fn tiles.reveal [location]
  (each [index t (ipairs hexes)]
    (when (= t.location location)
      (tset t :visible true)
      )))

(fn tiles.moving-hook [player-state index]
  (when (= player-state :done)
    (set state :targeting)
    )
  (when (not (. hexes index :visible))
    (tset (. hexes index) :visible true))
  )

(fn tiles.keypressed [key keycode]
  (match key
    := (ui.tme))
  (when ui.map-edit
    (match key
      :1 (do (pp "loading map") (load-map :map-1))
      :2 (do (pp "loading map") (load-map :map-2))
      :3 (do (pp "loading map") (load-map :map-3))
      :s (do (pp "saving map") (save :sample.fnl))
      )
    )
  )

(tset tiles :hexes hexes)

(tset tiles :tavern tavern)

tiles
