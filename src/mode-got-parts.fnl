(local fade (require :lib.fade))

(fn update [gs dt])

(local flux (require :lib.flux))
(local ship {:x 1000})
(local ship-move (flux.to ship 10 {:x -700}))
(ship-move:ease :linear)

(fn enter [gs]
  (fade.in))

(fn draw [gs]
  (lg.clear colours.white)
  (lg.setColor colours.brown)
  (lg.translate 0 20)
  (lg.setFont title-font)
  (lg.printf "You Escape" 0 0 1000 :center)
  (lg.translate 0 100)
  (lg.setColor [1 1 1 1])
  (lg.push)
  (lg.translate ship.x 0)
  (lg.draw assets.sprites.skyship 0 0)
  (lg.translate 1000 150)
  (local (w h) (values 400 55))
  (var (forcolour backcolour) (values colours.brown [1 1 1 0]))
  (local (mx my) (love.mouse.getPosition))
  (local (screen-mx screen-my) (lg.inverseTransformPoint mx my))
  (local hover (_G.pointWithin screen-mx screen-my 0 0 w h))
  (when hover
    (set (forcolour backcolour) (values colours.white colours.brown))
    (love.event.push :hover :quit)
    (when (and hover (love.mouse.isDown 1))
      (love.event.push :click :quit)))
  (lg.setColor backcolour)
  (lg.rectangle :fill 0 0 w h)
  (lg.setColor forcolour)
  (lg.rectangle :line 0 0 w h)
  (lg.setFont subtitle-font)
  (lg.printf "Quit" 0 0 w :center)
  (lg.pop)
  (lg.setColor colours.brown)
  (lg.translate 0 400)
  (lg.setFont subtitle-font)
  (lg.printf "Thanks to the help of your tenacious party you have gathered all the parts needed to escape this frozen hellscape" 100 0 800 :center)
  (lg.translate 300 250)
  (lg.origin)
  (lg.setColor 1 1 1 1)
  (lg.draw fade.canvas)
)


{: draw : update : enter}
