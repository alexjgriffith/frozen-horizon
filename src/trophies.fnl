{ ;; trophy
 :boars-head
 {
  :name "Boar's Head"
  :description "The head of a massive boar. It will help attract more adventurous adventurers."
  :icon :trophy
  :type :trophy
  :rarity :one
  :level 1
  :params {:target-level 1}
  }

 :mangled-centerpiece
 {
  :name "A Mangled Centrepiece"
  :description "This is true trash. It won't help attract any adventurers."
  :icon :trophy
  :type :trophy
  :rarity :common
  :level 1
  :params {:target-level 0}
  }

 
 :badger-art
 {
  :name "A Badger's Art"
  :description "It's not great for a badger. It will help attract more adventurous adventurers."
  :icon :trophy
  :type :trophy
  :rarity :one
  :level 1
  :depends {:quest true}
  :params {:target-level 1}
  }


 :ship-icon
 {
  :name "Your Ship's Icon"
  :description "Torn from the front of your crashed ship. It will help attract more adventurous adventurers."
  :icon :trophy
  :type :trophy
  :rarity :one
  :level 2
  :depends {:quest true}
  :params {:target-level 1}
  }


 :mouse-statue
 {
  :name "Mouse Statue"
  :description "A tiny statue of the Mouse God. It will help attract more adventurous adventurers."
  :icon :trophy
  :type :trophy
  :rarity :one
  :level 2
  :depends {:quest true}
  :params {:target-level 1}
  }


 :melted-wall
 {
  :name "Melted Wall Piece"
  :description "A melted section of the Frozen Fortress' wall. It will help attract more adventurous adventurers."
  :icon :trophy
  :type :trophy
  :rarity :one
  :level 4
  :depends {:quest true}
  :params {:target-level 1}
  }


 :duke-painting
 {
  :name "A Painting of the Frozen Duke"
  :description "This painting sends chills down the spines of every gnome. It will help attract more adventurous adventurers."
  :icon :trophy
  :type :trophy
  :rarity :one
  :level 7
  :depends {:quest true}
  :params {:target-level 1}
  }


 :party-statue
 {
  :name "A Statue of Your Party"
  :description "It looks nothing like them. It will help attract more adventurous adventurers."
  :icon :trophy
  :type :trophy
  :rarity :one
  :level 7
  :depends {:quest true}
  :params {:target-level 1}
  }
}
