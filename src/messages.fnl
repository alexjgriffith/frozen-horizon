(local flux (require :lib.flux))
(local {: info : wiggles} (require :state))

(local message-delay 5)

(local message-board {})
(set message-board.flux (flux.to wiggles 0.2 {:message-y 0}))

(fn message-board.open []
  (message-board.flux:stop)
  (set message-board.flux
       (flux.to wiggles 0.2 {:message-y 60}))
  (message-board.flux:oncomplete message-board.pause))

(fn message-board.close []
  (message-board.flux:stop)
  (set message-board.flux
       (flux.to wiggles 1 {:message-y 0})))

(fn message-board.pause []
  (message-board.flux:stop)
  (set message-board.flux
       (flux.to wiggles 0.2 {:message-y 0}))
  (message-board.flux:delay message-delay)
  (message-board.flux:oncomplete message-board.close))

(var output "")
(var fontColor colours.brown)
(fn write [message]
  (set output message)
  (message-board.open)
  (set fontColor colours.brown)
  true
  )

(fn error [message]
  (set output message)
  (message-board.open)
  (set fontColor colours.red)
  true
  )

(fn draw []
  (local (w h) (values 600 50))
  ;; (pp wiggles)
  (local (x y) (values (/ (- 900 w) 2) (- 720 wiggles.message-y)))
  (lg.push :all)
  (lg.setColor colours.white)
  (lg.rectangle :fill x y w h)
  (lg.setColor colours.brown)
  (lg.setLineWidth 6)
  (lg.rectangle :line x y w h)
  (lg.setLineWidth 1)
  (lg.setColor fontColor)
  (lg.printf output (+ x 10) (+ y 10) (- w 20) :center)
  (lg.pop)
  )

{: write : error : draw}
