(local gamestate (require :lib.gamestate))
(local tiles (require :tiles))
(local party (require :party))
(local item (require :item))
(local duke (require :frozen-duke))
(local interface (require :interface))
(local actions (require :actions))
(local fade (require :lib.fade))
(local messages (require :messages))
(local combat (require :combat-logic))
(local utilities (require :utilities))

(fn love.handlers.set-path [path]
  (party.path-hook path))

(fn love.handlers.party-moving [state index]
  (local {: info} (require :state))
  (local {: tile-details} (require :interface))
  (when (~= state :start) (tset info :moves (- info.moves 1)))
  (when (= state :done)
    (tile-details.open index)
    )
  (tiles.moving-hook state index))


(fn love.handlers.hover [what id]
  (local {: hover : info} (require :state))
  (tset hover :active true)
  (when (~= what hover.previous)
    (tset hover :previous what)
    (local sound assets.sounds.rpg.bookFlip2)
    (if (~= what :item)
      (do (sound:stop)
          (sound:play))
      (let [interface (require :interface)]
        ;; (pp [:popup-open id])
        ;; (interface.popup.open id)
        (love.mouse.setCursor selection-cursor)
        (tset info :hover-item id)
        )
      )))

(local clicks
       {:next-day
        
        (fn [x]
          (local {: info} (require :state))
          (if
           (and (party.empty) (= 1 info.day))
           (do
                (messages.write "Hire a Gnome from the Tavern.")
                )
           (= :waiting party.state)
              (do
                (assets.sounds.page:stop)
                (assets.sounds.page:play)
                (local {: info} (require :state))
                (local {: active-event} (require :state))
                (duke.next-day)
                (interface.tavern.next-day)
                (party.next-day)
                (tset active-event :action false)
                (tset active-event :event false)
                (tset info :day (+ info.day 1))
                (tset info :moves 4)
                (tset info :actions 1)
                (combat.zero)
                (interface.log.close)
                (actions.next-day party.index)
                )              
              (do
                (messages.error "party-moving")
                )
              )
          )

        :mute
        (fn [x]
          (ui.toggle-sound))

        :menu
        (fn [x]
          (local interface (require :interface))
          (interface.menu.toggle)
          )

        :duke
        (fn [x]
          (interface.dukeinfo.toggle)
          )

        :credits
        (fn [x]
          (local interface (require :interface))
          (interface.credits.open)
          )

        :log
        (fn [x]
          (local interface (require :interface))
          (interface.log.toggle)
          )
        
        :quit
        (fn [x]
          (love.event.quit)
          )

        :link
        (fn [x]
          (love.system.openURL x))

        :party
        (fn [x]
          (local interface (require :interface))
          (interface.tile-details.toggle party.index :force-open))

        :tavern
        (fn [x]
          (local interface (require :interface))
          (interface.tavern.toggle)
          )

        :reset
        (fn []
          (messages.error :not-implemented)
          )
        
        :item
        (fn [[item-id where]]
          (local item (require :item))
          ;; pickup
         (when (and item-id)
           (item.move-to-hand item-id))
         )
        
        :toggle-casual-mode (fn [] (utilities.toggle-easy-mode))
        :x
        (fn [interface-name]
          (local interface (require :interface))
          (local i (. interface interface-name))
          (i.close)
          )

        :action
        (fn [[action-object tile-index]]
          (local state (require :state))
          (tset state :active-event
                (lume.clone (actions.active-event action-object tile-index)))
          (tset state.info :moves 0)
          (when interface.tile-details.active
               (interface.tile-details.close)
               (interface.tile-details.open tile-index :force))
          )

        :targeted-gnome
        (fn [g]
          (local interface (require :interface))
          (interface.tavern.focus-on-gnome g))

        :hire
        (fn [gnome-id]
          (local {: info} (require :state))
          (local gnome (require :gnome))
          (local interface (require :interface))
          (local g (gnome.get-gnome gnome-id))
          (if (>= info.gold g.cost)
              (if (gnome.from-tavern-to-party gnome-id)
                  (do (tset info :gold (- info.gold g.cost))
                      (interface.tavern.focus-on-gnome nil))
                  (tset info :error "Not enough space in the party")
                  )
              (tset info :error "Not enough gold")
              )
          (pp info.error)
          )

        :focus-gnome-party
        (fn [party-id]
          (local {: hover} (require :state))
          (tset hover :focused-gnome-party-index party-id)
          )

        :unfocus-gnome-party
        (fn []
          (local {: hover} (require :state))
          (tset hover :focused-gnome-party-index false)
          )

        :fire-gnome
        (fn [party-id]
          (local {: hover} (require :state))
          (party.remove-gnome party-id)
          (tset hover :focused-gnome-party-index false)
          )

        :play
        (fn []
          (fade.out (fn []
                      (gamestate.switch (require :mode-story)))))

        :continue
        (fn []
          (fade.out (fn []
                      (gamestate.switch (require :mode-map))))
          )

        :combat-log-result
        (fn []
          (messages.error :not-implemented))
       }
       )


(fn love.handlers.click [what info]
  (local {: hover} (require :state))
  (when (not hover.click-down)
    (tset hover :click-down true)
    (local sound  assets.sounds.click)
    (sound:stop)
    (sound:play)
    (when (. clicks what)
      ((. clicks what) info)))
  )

(fn love.handlers.item-in-hand-at [where index]
  (local {: items} (require :state))
  (tset items :over where))

(fn love.handlers.use [item-id type params]
  (assets.sounds.rpg.bookOpen:play)
  (match type
    :map (let [interface (require :interface)]
           (interface.close-all)
           (tiles.reveal params.location)
           (messages.write "Location Revealed")
           (item.move-by-id item-id :used)
           )
    :quest (do
             (local party (require :party))
             (local tile-index party.index)
             (local {: quests} (require :state))
             (local {: location : action : prize} params)
             (tset quests location {: location : action : prize : item-id})
             (when interface.tile-details.active
               (interface.tile-details.close)
               (interface.tile-details.open tile-index :force)
               )
             (messages.write "Quest Added")
             (item.move-by-id item-id :used)
             )
    :missinformation (do
                       (messages.write "Missinformation Spread")
                       (duke.missinform params.duke-vision)
                       (item.move-by-id item-id :used)))
  )

(fn love.handlers.game-over [ending]
  (fade.out (fn []
              (match ending
                :duke-kill (gamestate.switch (require :duke-kill-mode))
                :got-parts (gamestate.switch (require :mode-got-parts))
                )
              )))

(fn love.handlers.combat []
  (assets.sounds.rpg.knifeSlice:play)
  (interface.log.open))
