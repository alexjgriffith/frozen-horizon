{
 :sail
 {
  :name "Airship Sail"
  :description "A massive sail designed for catching cool breezes"
  :icon :part-1
  :type :part
  :rarity :one
  :level 1
  :depends {:quest :the-airship}
  :cant-sell true
  :params {}
  }

:mast
 {
  :name "Makeshift airship Mast"
  :description "This used to be part of a church, but it makes a good mast"
  :icon :part-2
  :type :part
  :rarity :one
  :level 1
  :depends {:quest :the-church}
  :cant-sell true
  :params {}
  }


 :lightener
 {
  :name "Airship Lightening Gas"
  :description "The gas needed to keep your airship adrift"
  :icon :part-3
  :type :part
  :rarity :one
  :level 1
  :depends {:quest :the-mages-tower}
  :cant-sell true
  :params {}
  }

}
