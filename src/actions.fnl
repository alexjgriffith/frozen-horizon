(local actions {})

(tset actions :list
      {
       ;; Base actions available
       :search-wild {:name ["Rummage" "Scrounge" "Inspect"]
                     :description "Your party rummages around looking for treasure"
                     :events {:find-nothing 0.3
                              :find-normal-treasure 0.3
                              :find-good-treasure 0.1
                              :attacked-by-normal 0.2
                              :attacked-by-strong 0.1}}

       
       :rest-wild {:name ["Make Camp"]
                   :description "Your party takes the time to make camp"
                   :events {:fail-to-make-camp 0.5
                            :make-camp 0.5}
                   }

       :search-town {:name ["Browse About" "Go Shopping"]
                     :description "Your party rummages around looking for treasure"
                     :events {:find-nothing 0.1
                              :find-normal-treasure 0.8
                              :attacked-by-normal 0.1
                              }}
       
       :rest-town {:name ["Look for an Inn for the Night"]
                   :description "Your party searches for a nice warm place to sleep"
                   :events {:fail-to-find-inn 0.1
                            :find-inn 0.9}
                   }

       ;; Location specific activities
       :listen-to-music {:name ["Listen to Music"]
                         :description "Your party rests for a time to listen to music"
                         :events {:listen-to-music 0.9
                                  :attacked-by-normal 0.1}
                         }

       :go-to-library {:name ["Go to the Library"]
                       :description "Your party searches for a library  for information"
                       :events {:library 0.9
                                :attacked-by-normal 0.1}
                         }

       :visit-armoury {:name ["Visit Blacksmith" "Goto the Armoury"]
                       :description "Your party searches for armour and weapons"
                       :events {:armoury 0.9
                                :attacked-by-normal 0.1}
                       }

       :frolic {:name ["Frolic under the branches" "Dance around the meadow"]
                       :description "Your party frolics around"
                       :events {:armoury 0.9
                                :attacked-by-normal 0.1}
                }

       :chat-up-mage {:name ["Seek Potions" "Chat up the Mage"]
                :description "Your party frolics looks for potions"
                :events {:find-potions 0.9
                         :attacked-by-normal 0.1}
                }

       :explore-wreckage {:name ["Search the Remains"]
                          :description "Your party digs through the remains of the airship wreckage"
                          :events {:nothing 0.1
                                   :find-good-treasure 0.8
                                   :attacked-by-normal 0.1}}

       :get-directions {:name ["Get Directions" "Chat up Locals"]
                        :description "Your party goes around the village asking locals for directions"
                        :events {:nothing 0.2
                                 :directions 0.8
                                 :attacked-by-normal 0.1}}

       :hide-from-duke {:name ["Hide from the Duke"]
              :description "Your party is hiding from the Frozen Duke"
              :events {:nothing 0.1
                       :hide-from-duke 0.4
                       :attacked-by-normal 0.4
                       :attacked-by-strong 0.1}}
       
       :delve {:name ["Delve too Deep"]
               :description "Your party delves deeply and greedily"
               :events {:nothing 0.2
                        :delve 0.6
                        :attacked-by-normal 0.3}}
       
       ;; Next day activities
       :next-day-town {:name ["A New Day Dawns"]
                       :description "Your party awakens in town"
                       :events {:nothing-overnight 0.8
                                :attacked-by-normal 0.2
                                }}

       :next-day-tavern {:name ["A New Day Dawns"]
                       :description "Your party awakens in the tavern"
                       :events {:nothing-overnight 1
                                }}
       
       :next-day-wild {:name ["A New Day Dawns"]
                       :description "Your party awakens in the wilderness"
                       :events {:nothing-overnight 0.5
                                :attacked-by-normal 0.5
                                }}

       ;; test 
       :test-message {:name ["Frolic about"]
                      :description "Your party frolics about"
                      :events {:frolic 1}
                      }
       
       :test-get-items {:name ["Rummage"]
                      :description "Your party rummages around"
                      :events {:find-normal-treasure 1}
                      }

       :test-combat {:name ["Practice swordsmanship"]
                      :description "Your party puts their combat skills to the test."
                      :events {:attacked-by-normal 1}
                      }

       :test-rest {:name ["Take a Relaxing Bath"]
                      :description "Your party takes a relaxing bath"
                      :events {:find-inn 1}
                      }       
       ;; Quest actions
       :the-wild-badger {:name ["Easing the badger"]
                         :description "Your party tries to solve a badger problem"
                         :events {:solve-badger-problem 0.9
                                  :fail-badger-problem 0.1}}

       :needs-quest {:name ["The Mayor of Needs"]
                         :description "The Mayor of Subzero has been gnome napped. Your party leaps into action."
                         :events {:solve-needs-quest 1}}

       :airship-quest {:name ["Extract Ship Icon"]
                         :description "Your party tries to detatch the ships icon"
                         :events {:solve-airship-quest 0.9
                                  :fail-airship-quest 0.1}}

       :touch-quest {:name ["Make Shoes For Mice"]
                     :description "Your party tries to knit shoes for the frozen mice"
                     :events {:solve-touch-quest 0.9
                              :fail-touch-quest 0.1}}

       :tower-quest {:name ["Commune with Fireflies"]
                         :description "Your party tries to engage the local Fireflies"
                         :events {:solve-tower-quest 0.9
                                  :fail-tower-quest 0.1}}

       :hills-quest {:name ["Stop the Slide"]
                         :description "Your party tries to add traction to the Slippery Slopes"
                         :events {:solve-hills-quest 0.9
                                  :fail-hills-quest 0.1}}
       
       :hideout-quest {:name ["Indulge Gnomic Curiosity"]
                       :description "Your party climbs up the mountain to investigate the house"
                       :events {:solve-hideout-quest 0.9
                                :fail-hideout-quest 0.1}}

       :frosthaven-quest {:name ["Engage in Mysticism"]
                       :description "Your party reaches out to dissafected gnomes in Frosthaven trying to understand their plight"
                       :events {:solve-frosthaven-quest 1}}

       :colderado-quest {:name ["Find Source of Wails"]
                       :description "Your party searches through the streets of Colderado looking for the source of the eerie wails"
                       :events {:solve-colderado-quest 1}}
       
       :mage-quest {:name ["Ground Mage-or Tom"]
                     :description "Your party tries to ground mage-or tom"
                     :events {:solve-mage-quest 1}}

       :church-quest {:name ["Solve the Cross Problem"]
                       :description "Your party tries to help decide what to do with the old cross"
                       :events {:solve-church-quest 1}}

       :fortress-quest {:name ["Steal Flag for Sail"]
                       :description "Your party tries to steel the Frozen Dukes flag"
                       :events {:solve-fortress-quest 1}}
       })

(fn actions.active-event [action tile-index]
  (local events (require :events))
  (local event-name (lume.weightedchoice action.events))
  (local party (require :party))
  (local event (if (not (party.empty))
                   (let [e (events.call event-name tile-index action.description)]
                     e)
                   false))
  {: action :event (or event false) : event-name}
  )

(fn actions.next-day [tile-index]
  (local town-types {:frozenduke 1 :church 1 :village 1 :town 1 :city 1 :magetower 1})
  (local party (require :party))
  
  (local events (require :events))
  (local {: hexes} (require :state))
  (local locations (require :locations))
  (local location-name (or (?. hexes tile-index :location) :wilderness))
  (local location-details (locations.get-details location-name))
  (local in-tavern? (= :tavern location-details.type))
  (local in-town? (= 1 (. town-types location-details.type)))
  (local action-name (if in-town? :next-day-town in-tavern? :next-day-tavern :next-day-wild))
  (local action (. actions.list action-name ))
  (local ret 
         (actions.active-event action tile-index))
  (party.reset-defense-camp-bonus)
  ret
  )

actions
