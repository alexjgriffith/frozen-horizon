(local flux (require :lib.flux))
(local {: info : wiggles} (require :state))

(local message-board {})
(set message-board.flux (flux.to wiggles 0.2 {:hex-location-y 0}))

(fn message-board.open []
  (message-board.flux:stop)
  (set message-board.flux
       (flux.to wiggles 0.2 {:hex-location-y 60})))

(fn message-board.close []
  (message-board.flux:stop)
  (set message-board.flux
       (flux.to wiggles 1 {:hex-location-y 0})))

(var output "")
(var fontColor colours.brown)
(local locations (require :locations))
(fn update [_dt]
  (if info.hex-location
      (do
        (set output (. locations.list info.hex-location :name))
        (message-board.open))
      (message-board.close)
      )
  )

(fn draw []
  (local (w h) (values 600 50))
  ;; (pp wiggles)
  (local (x y) (values (/ (- 900 w) 2) (+ -70  wiggles.hex-location-y)))
  (lg.push :all)
  (lg.setColor colours.white)
  (lg.rectangle :fill x y w h)
  (lg.setColor colours.brown)
  (lg.setLineWidth 6)
  (lg.rectangle :line x y w h)
  (lg.setLineWidth 1)
  (lg.setColor fontColor)
  (lg.printf output (+ x 10) (+ y 10) (- w 20) :center)
  (lg.pop)
  )

{: update : draw}
