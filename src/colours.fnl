(local c (require :lib.colour))

(local cc-29-descriptions
       {:white"#f2f0e5"
        :grey "#b8b5b9"
        :grey2 "#868188"
        :grey3 "#646365"
        :grey4 "#45444f"
        :dark-blue "#3a3858"
        :black "#212123"
        :shadow "#352b42"
        :blue "#43436a"
        :bright-blue "#4b80ca"
        :teal "#68c2d3"
        :aqua "#a2dcc7"
        :yellow "#ede19e"
        :orange "#d3a068"
        :red "#b45252"
        :light-purple "#6a536e"
        :dark-purple"#4b4158"
        :brown "#80493a"
        :light-brown "#a77b5b"
        :tan "#e5ceb4"
        :light-green "#c2d368"
        :green "#8ab060"
        :bluegreen "#567b79"
        :dark-green "#4e584a"
        :olive "#7b7243"
        :pale-green "#b2b47e"
        :pale-pink "#edc8c4"
        :bright-pink "#cf8acb"
        :dark-pink "#5f556a"})

(local colours (collect [key hex (pairs cc-29-descriptions)] (values key (c.hex-to-rgba hex))))

colours
