{ ;; maps
 :town-map-1
 {
  :name "Map to Nearby Town"
  :description "A map that will reveal the location of Iceville, a nearby town."
  :icon :map
  :type :map
  :rarity :one
  :level 1
  :params {:location :town-1}
  }

 :town-map-2
 {
  :name "Map to Nearby Town"
  :description "A map that will reveal the location of Subzero, a nearby town."
  :icon :map
  :type :map
  :rarity :one
  :level 1
  :params {:location :town-2}
  }


  :city-map-1
 {
  :name "Map to Nearby City"
  :description "A map that will reveal the location of Frosthaven, a nearby city."
  :icon :map
  :type :map
  :rarity :one
  :level 3
  :params {:location :city-1}
  }

 :city-map-2
 {
  :name "Map to Nearby City"
  :description "A map that will reveal the location of Colderado, a nearby city."
  :icon :map
  :type :map
  :rarity :one
  :level 3
  :params {:location :city-2}
  }


 :fortress-map
 {
  :name "Map to The Fortress"
  :description "A map that will reveal the location of a nearby fortress."
  :icon :map
  :type :map
  :rarity :one
  :level 4
  :params {:location :fortress}
  }


 :mages-tower-map
 {
  :name "Map to The Mages Tower"
  :description "A map that will reveal the location of the mages tower."
  :icon :map
  :type :map
  :rarity :one
  :level 6
  :params {:location :fancy-tower}
  }

 :church-map
 {
  :name "Map to The Church"
  :description "A map that will reveal the location of the church."
  :icon :map
  :type :map
  :rarity :one
  :level 2
  :params {:location :church}
  }

 :trash-map
 {
  :name "A Damaged Map"
  :description "This map is too damaged to read"
  :icon :map
  :type :map
  :rarity :common
  :level 1
  :params {:location :tavern}
  }
}
