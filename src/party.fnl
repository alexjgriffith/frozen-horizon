(local flux (require :lib.flux))

(local {: party} (require :state))

(local max-party-members 6)

(fn party.start [index w1 w2]
  (local hex (require :hex))
  (local (i j) (hex.index-to-ij index w1))
  (local (x y) (hex.ij-to-xy i j w2))
  (tset party :home-x x)
  (tset party :home-y y)
  (tset party :home-index index)
  (tset party :index index)
  (tset party :x x)
  (tset party :y y))

(fn reverse [t]
  (local ret [])
  (local l (# t))
  (for [i 1 l]
    (tset ret (+ ( - l i) 1) (. t i))
    )
  ret)

(fn party.reset-hook []
  ;; (pp :reset-hook)
  (tset party :x party.home-x)
  (tset party :y party.home-y)
  (tset party :index party.home-index)
  (tset party :state :waiting)
  (tset party :direction :out)
  (tset party :path-index 1)
  (tset party :path [])
  (when party.flux (party.flux:stop)))

(fn party.add-gnome [id]
  (when (and id (< (# party.members) max-party-members))
    (table.insert party.members id)
    (local messages (require :messages))
    (local {: gnomes} (require :state))
    (local gnome-name (?. gnomes.list id :name))
    (when gnome-name
      (messages.write (.. gnome-name " joined the party")))
    true))

(fn shift-items-down [removed len]
  (local {: items} (require :state))
  (local inventories items.inventories)
  (for [i removed len]
    (let [e (.. :e i)
          eup (.. :e (+ i 1))]
      (lume.clear (. inventories e))
      (each [_ te (ipairs (. inventories eup))]
        (table.insert (. inventories e) te)
        )
      ))
  (lume.clear (. inventories (.. :e (+ len 1))))
  )

(fn print-items []
  (local {: items} (require :state))
  (each [_ n (ipairs [:e1 :e2 :e3 :e4 :e5 :e6])]
    (local inv (. items :inventories n))
    (var pl [n])
    (each [ _ i (ipairs inv)]
      (table.insert pl (. items :list i :name)))
    (db pl :party-items)
    )
  )

(fn party.remove-gnome [party-id]
  ;; (var id (. party.members party-id))
  (table.remove party.members party-id)
  ;; (print-items)
  (shift-items-down party-id (# party.members))
  (when (= 0 (# party.members))
    (local {: info} (require :state))
    (tset info :moves 0))
  )

(fn party.path-hook [path]
  (when (> (# party.members) 0)
    (tset party :path (reverse path))
    (when (> (# party.path) 1)
      (set party.state :start)
      (set party.direction :out)
      (set party.path-index 2)
      (set party.x (. party.path 1 1))
      (set party.y (. party.path 1 2)))))

(fn next-move []
  (if (and (< party.path-index (# party.path)) (= party.direction :out))
      (do (set party.state :start)
          (set party.path-index (+ party.path-index 1))
          (love.event.push :party-moving :out (. party.path party.path-index 3))
          )
      (= party.path-index (# party.path))
      (do (set party.state :waiting)
          (love.event.push :party-moving :done (. party.path party.path-index 3))
          )
      )
  (tset party :index (. party.path party.path-index 3))
  )

(fn party.update [dt]  
  (match party.state
    :start (do
             ;; (set party.path-index (+ party.path-index 1))
             (tset party :flux (flux.to party 1 {:x (. party.path party.path-index 1)
                                                 :y (. party.path party.path-index 2)}))
             (party.flux:oncomplete next-move)
             (party.flux:ease :quadinout)
             (love.event.push :party-moving :start (. party.path party.path-index 3))
             (tset party :state :moving))
    :moving (do
              :nothing)
    )
  ;; remove dead gnomes
  (local gnome (require :gnome))
  (local dead-gnomes [])
  (each [i gnome-index (ipairs party.members)]
    (gnome.update gnome-index)
    (when (gnome.dead gnome-index)
      (table.insert dead-gnomes i)))
  (each [_ dead-gnome (ipairs dead-gnomes)]
    (party.remove-gnome dead-gnome))
  
  (when (party.empty)
    (party.reset-hook))
  )

(fn party.empty []
  (= 0 (# (. party :members))))

(fn party.at-home []
  (= party.index party.home-index))

(fn party.get-index []
  (. party :index))

(fn party.get-gnome-id [gnome-index]
  (. party :members gnome-index))

(fn party.get-members []
  (local {: gnomes} (require :state))
  (icollect [_ gnome-index (ipairs (. party :members))]
    (. gnomes :list gnome-index)))

(fn party.damage-members [by]
  (local {: gnomes} (require :state))
  (each [_ gnome-index (ipairs (. party :members))]
    (local g (. gnomes :list gnome-index))
    (tset g :health (math.max 0 (- g.health by)))))

(fn party.moved []
  (local {: info} (require :state))
  (~= info.moves 4))

(fn party.outdoors []
  (local {: hexes} (require :state))
  (not (. hexes party.index :location)))

(fn party.next-day []
  (local gnome (require :gnome))
  (local dead-gnomes [])
  (each [i gnome-index (ipairs party.members)]
    (gnome.next-day gnome-index)
    (when (gnome.dead gnome-index)
      (table.insert dead-gnomes i)))
  (each [_ dead-gnome (ipairs dead-gnomes)]
    (party.remove-gnome dead-gnome))

  ;; check win condition
  (local  {: items} (require :state))
  (local item-map {})
  (each [_ item-id (ipairs items.inventories.party)]
    (local item (. items.list item-id))
    (tset item-map item.handle true))
  
  (when (and (. item-map :sail) (. item-map :mast) (. item-map :lightener))
    (love.event.push :game-over :got-parts)
    )
  )

;; do this after the new-day action
(fn party.reset-defense-camp-bonus []
  (local gs (party.get-members))
  (each [i g (ipairs gs)]
    (tset g :defense-camp-bonus 0)
  ))

(fn party.draw []
  (local {: gnomes} (require :state))
  (local gnome (require :gnome))
  (lg.push)
  (lg.setColor 1 1 1 1)
  (lg.translate (- party.x 23) (- party.y 24))
  (local count (# party.members))
  (local radius (match count
                        1 0
                        2 6
                        3 6
                        4 8
                        5 10
                        _ 10))
  (local translations [])
  (for [index 1 count]
    (local angle (+ 0.12 (* (- (- index 1)) (/ (* 2 math.pi) count))))
    (table.insert translations [(* radius (math.cos angle))
                                (* radius (math.sin angle))])
                         
    )
  (local sorted-translations
         (lume.sort translations (fn [a b] (< (. a 2) (. b 2)))))
  (each [index gnome-id (ipairs party.members)]
    (local [x y] (. sorted-translations index))
    (local g (. gnomes.list gnome-id))
    (when g
      (gnome.draw g x y))
    )
  (lg.pop))

party
