(fn toggle-easy-mode []
  (local {: info} (require :state))
  (if (not info.easy-mode)
      (do
        (tset info :fight-value-reduction 3)
        (tset info :base-rate 2)
        (tset info :easy-mode true)
        )
      (do
        (tset info :fight-value-reduction 0)
        (tset info :base-rate 4)
        (tset info :easy-mode false)
        )))

{: toggle-easy-mode}
