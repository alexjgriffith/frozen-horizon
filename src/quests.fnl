{;; quests
 :town-1-quest
 {
  :name "The Wild Badger"
  :description "The Wild Badger Inn in Iceville is having an unironic badger problem."
  :icon :paper
  :type :quest
  :rarity :one
  :cant-sell true
  :depends {:location :town-1}
  :level 2
  :params {:location :town-1 :action :the-wild-badger}
  ;; grants trophy
  }

 :village-quest
 {
  :name "Losing Touch"
  :description "The mice of the local village have been getting frostbite on their paws."
  :icon :paper
  :type :quest
  :rarity :one
  :cant-sell true
  :depends {:location :village}
  :level 2
  :params {:location :village :action :touch-quest}
  ;; grants trophy
  }

 :tower-quest
 {
  :name "Holes in Holes"
  :description "The fireflies have been at the towers of the Frozen Fortress melting its outer walls."
  :icon :paper
  :type :quest
  :rarity :one
  :cant-sell true
  :depends {:location :tower}
  :level 4
  :params {:location :tower :action :tower-quest}
  ;; grants trophy
  }

 :hills-quest
 {
  :name "Traction Trouble"
  :description "Gnomes have been slipping and hurting themselves on the Slippery Hills."
  :icon :paper
  :type :quest
  :rarity :one
  :cant-sell true
  :depends {:location :hills}
  :level 4
  :params {:location :hills :action :hills-quest}
  ;; grants trophy
  }

 :hideout-quest
 {
  :name "What's in the Notch"
  :description "Mysteries abound about Smuggler's Notch. Some of them must be real, right?"
  :icon :paper
  :type :quest
  :rarity :one
  :cant-sell true
  :depends {:location :hideout}
  :level 4
  :params {:location :hideout :action :hideout-quest}
  ;; grants trophy
  }

 :airship-quest
 {
  :name "Airship Wreckage"
  :description "The wreckage of your airship can be seen from The Friggin Cold Tavern."
  :icon :paper
  :type :quest
  :rarity :one
  :cant-sell true
  :depends {:location :airship}
  :level 1
  :params {:location :airship :action :airship-quest}
  ;; grants trophy
  }
 
 :town-2-quest
 {
  :name "Gnome Napping"
  :description "The mayor of Subzero has been kidnapped by the Frozen Duke's Cronies."
  :icon :paper
  :type :quest
  :rarity :one
  :cant-sell true
  :depends {:location :town-2}
  :level 1
  :params {:location :town-2 :action :needs-quest}
  ;; leads to fortress-quest
  }

 :city-1-quest
 {
  :name "Haven and Hearth"
  :description "A gnome from Frosthaven has lost his way from the fold."
  :icon :paper
  :type :quest
  :rarity :one
  :cant-sell true
  :depends {:location :city-1}
  :level 2
  :params {:location :city-1 :action :frosthaven-quest}
  ;; leads to church-quest
  }

 :city-2-quest
 {
  :name "The Wails of Whales"
  :description "The gnomes of Colderado are being haunted by an eerie wailing."
  :icon :paper
  :type :quest
  :rarity :one
  :cant-sell true
  :depends {:location :city-2}
  :level 3
  :params {:location :city-2 :action :colderado-quest}
  ;; leads to mage-quest
  }

 :fortress-quest
 {
  :name "Where are all the Flags"
  :description "The Mayor of Subzero overheard that the Frozen Duke has just issued an edict that all flags be brought to his haunt."
  :icon :paper
  :type :quest
  :rarity :one
  :cant-sell true
  :depends {:quest true}
  :level 8
  :params {:location :fortress :action :fortress-quest}
  ;; grants sail
  }

 :church-quest
 {
  :name "Crisis of Faith"
  :description "The gnomes of Frosthaven have become disillusioned with the monks of Our Frozen Grace. They believe they are hoarding too many crosses."
  :icon :paper
  :type :quest
  :rarity :one
  :cant-sell true
  :depends {:quest true}
  :level 8
  :params {:location :church :action :church-quest}
  ;; grants mast
  }

 :mage-quest
 {
  :name "Down to Earth"
  :description "The gnome mage known as Tom has been floating away from The Mage's Lookout. He needs a hand coming back down to earth."
  :icon :paper
  :type :quest
  :rarity :one
  :cant-sell true
  :depends {:quest true}
  :level 8
  :params {:location :fancy-tower :action :mage-quest}
  ;; grants lightener
  }



}
