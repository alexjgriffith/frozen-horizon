(local icons (require :icons))
(local characters (require :characters))

(local gnome {})

(set gnome.names (lume.split
                  (love.filesystem.read :/assets/data/names.txt) "\n"))

(local {: gnomes} (require :state))

(fn gnome.draw [gnome x y]
  (icons.draw (match gnome.class
                :warrior :gnome-3
                :archer :gnome-2
                :mage   :gnome-1
                ) x y))

(fn gnome.draw-sprite-by-id [gnome-id x y]
  (local g (. gnomes :list gnome-id))
  (when g
    (lg.setColor colours.brown)
    (lg.printf g.name -32 128 128 :center)
    (lg.printf (.. "Lvl:" g.level) -32 150 128 :center)
    (lg.setColor 1 1 1 1)
    (characters.draw g.sprite x y)))

(fn gnome.draw-sprite-by-id-no-info [gnome-id x y]
  (local g (. gnomes :list gnome-id))
  (lg.setColor [1 1 1 1])
  (when g (characters.draw g.sprite x y)))


(fn gnome.random-class []
  (match (math.random 3)
    1 :mage
    2 :warrior
    3 :archer))

(fn gnome.random-sprite [class]
  (.. class "-" (math.random 3)))

(fn gnome.attack-from-level [level class]
  (->
   (match class
     :mage 1.5
     :warrior 1.1
     :archer 0.9)
   (* level)
   (+ 1)
   (math.floor)
   (math.min 10)
   (math.max 1)))

(fn gnome.defense-from-level [level class]
  (->
   (match class
     :mage 0.6
     :warrior 1.5
     :archer 0.7)
   (* level)
   (+ 1)
   (math.floor)
   (math.min 10)
   (math.max 1)))

(fn gnome.fight-from-level [level class]
  (->
   (match class
     :mage    0.3
     :warrior 0.4
     :archer  1)
   (* level)
   (+ 1)
   (math.floor)
   (math.min 5)
   (math.max 1)))

(fn gnome.health-from-level [level class]
  (->
   (match class
     :mage    5
     :warrior 10
     :archer  7)
   (* level)
   (+ 30)
   (math.floor)
   (math.max 30)
   (math.min 100)))


(fn gnome.cost-from-level [level class]
  (->
   (match class
     :mage    70
     :warrior 50
     :archer  40)
   (* level)
   (+ 100)
   (math.floor)
   (math.max 30)
   (math.min 100000))
  )

(fn gnome.random-name []
  (local count (- (# gnome.names) 1))
  (. gnome.names (math.random count)))

(fn gnome.random-level [target-level]
  (math.random (math.max 1 (- target-level 2))
               (math.min 10 (+ target-level 1))))

(fn gnome.new [name class level equipment items]
  (local id gnomes.next-id)
  (set gnomes.next-id (+ gnomes.next-id 1))
  (local health (gnome.health-from-level level class))
  (local new-gnome   
          {: id
           : name
           : class
           : level
           : equipment
           : items
           :cost (gnome.cost-from-level level class)
           :sprite (gnome.random-sprite class)
           :attack (gnome.attack-from-level level class)
           :attack-bonus 0
           :potion-attack-bonus 0
           :potion-attack-bonus-days 0
           :defense (gnome.defense-from-level level class)
           :defense-bonus 0
           :defense-camp-bonus 0
           :potion-defense-bonus 0
           :potion-defense-bonus-days 0
           :fight-value (gnome.fight-from-level level class)
           :fight-value-bonus 0
           :potion-fight-value-bonus 0
           :potion-fight-value-bonus-days 0
           :health health
           :max-health health
           }
    )
  (tset gnomes.list id new-gnome)
  id
  )

(fn gnome.consume [gnome-id target value days]
  (local g (-> gnome-id gnome.get-gnome))
  (if (= target :health)
      (tset g :health (math.min g.max-health (+ g.health value)))
      (do (tset g target value)
          (tset g (.. target "-days") days))))

(fn gnome.equip [gnome-id type class value]
  (var class-match false)
  (local g (-> gnome-id gnome.get-gnome))
  (each [_ c (ipairs (or class []))]
    (when (= c g.class) (set class-match true)))
  (when class-match
    (match type
    :armour (tset g :defense-bonus value)
    :weapon (tset g :attack-bonus value)
    )
    true
    )
  )

(fn gnome.unequip [gnome-id type value]
  (local g (-> gnome-id gnome.get-gnome))
  (when g
    (match type
    :armour (tset g :defense-bonus 0)
    :weapon (tset g :attack-bonus 0)
    ))
  )

(fn gnome.new-random [target-level preferences?]
  (local item (require :item))
  (local level (gnome.random-level target-level))
  (local is [])
  (local count (math.random 1 4))
  (for [i 1 count]
    (local picked (item.pick level preferences? :gnome))
    (when picked (table.insert is picked)))
  (gnome.new (gnome.random-name)
             (gnome.random-class)
             level
             []
             is
             ))

(fn gnome.update [gnome-id]
  (local g (. gnomes.list gnome-id))
  ;; (pp g)
  )

(fn gnome.get-gnome [gnome-id]
  (. gnomes.list gnome-id))

(fn gnome.dead [gnome-id]
  (<= (. gnomes.list gnome-id :health) 0))

(fn gnome.from-tavern-to-party [gnome-id]
  (local party (require :party))
  (when (party.add-gnome gnome-id)
    (lume.remove gnomes.tavern gnome-id)
    true))

(fn gnome.next-day [gnome-id]
  (local g (gnome.get-gnome gnome-id))
  (fn decr-potion-bonus [value]
    (tset g (.. value "-days") (- (. g (.. value "-days")) 1))
    (when (<= (. g (.. value "-days")) 0)
      (tset g value 0)))
  (local potions ["potion-attack-bonus" "potion-defense-bonus" "potion-fight-value-bonus"])
  (each [_ p (ipairs potions)] (decr-potion-bonus p))
 
  )

gnome
