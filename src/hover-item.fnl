(local flux (require :lib.flux))
(local {: info : wiggles} (require :state))

(local message-board {})
(set message-board.flux (flux.to wiggles 0.2 {:hover-item-y 0}))

(fn message-board.open []
  (message-board.flux:stop)
  (set message-board.flux
       (flux.to wiggles 0.2 {:hover-item-y 130})))

(fn message-board.close []
  (message-board.flux:stop)
  (set message-board.flux
       (flux.to wiggles 0.5 {:hover-item-y 0}))
  ;; (message-board.flux:ease :linear)
  )

(var output false)
(fn update [_dt]
  (if info.hover-item
      (do
        (set output info.hover-item)
        (message-board.open))
      (message-board.close)))

(fn draw []
  ;;(local (x y) (values (/ (- 900 w) 2) (+ -70  wiggles.hover-item-y)))
  (when output
    (local (w h) (values 600 110))
    (local (x y) (values (/ (- 900 w) 2) (+ (- 10 130 ) wiggles.hover-item-y)))

    (local {: items} (require :state))
    (local item (require :item))
    (local item-id output)
    (local i (. items.list item-id))
    (when i
      (local {: name : description : cost : sell : handle
              : params : type : rarity : level : current-location} i)
      (lg.push :all)
      (lg.translate x y)
      (lg.setColor colours.white)
      (lg.rectangle :fill 0 0 w h)
      (lg.setColor colours.brown)
      (lg.setLineWidth 6)
      (lg.rectangle :line 0 0 w h)
      (lg.setLineWidth 1)
      (item.draw item-id 0 8)
      (lg.setFont big-button-font)
      (lg.setColor colours.brown)
      (lg.printf name 0 4 w :center)
      (lg.setFont button-font)    
      (local r (match rarity :one :unique :common :common :legendary :legenday _ ""))
      (assert r (.. "Item " handle " is missing rarity."))
      (lg.printf (.. r " " type) 0 30 w :center)
      (match current-location
        :party (lg.printf (.. sell " Gold") 0 30 (- w 10) :right)
        :gnome (lg.printf (.. cost " Gold") 0 30 (- w 10) :right))
      (lg.push)
      (lg.translate 10 55)
      (lg.setFont button-font)
      (lg.printf description 0 0 (- w 20) :left)
      (lg.pop)
      (lg.pop)))
  )

{: update : draw}
