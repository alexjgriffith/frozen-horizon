{:mode :mode-opening
 :party {:members []
         :x 0
         :y 0
         :state :waiting
         :direction :out
         :index 1
         :path []}
 :info {:gold 2000 :day 1 :duke -20
        :base-rate 4
        :fight-value-reduction 0
        :moves 4 :actions 1
        :next-day-lock false
        :easy-mode false
        :move-lock false
        :hover-item false
        :hex-location false}
 :wiggles {:next-day 0 :message-y 0 :hex-location-y 0 :hover-item-y 0}
 :hexes []
 :active-event {:event false :action false}
 :items {:list [] :inventories {:party []
                                :p1 []
                                :e1 []
                                :p2 []
                                :e2 []
                                :p3 []
                                :e3 []
                                :p4 []
                                :e4 []
                                :p5 []
                                :e5 []
                                :p6 []
                                :e6 []
                                :gnome []
                                :hand []
                                :use []
                                :used []
                                :location []
                                :tavern []
                                :trash []
                                :created []}
         :limits {:party 24 :gnome 6
                  :p1 1 :e1 2 :p2 1 :e2 2
                  :p3 1 :e3 2 :p4 1 :e4 2
                  :p5 1 :e5 2 :p6 1 :e6 2
                  :location 8
                  :tavern 8}
         :hand false :next-id 1 :over false}
 :hover {:active false :previous false :click-down false
         :focused-gnome-party-index false}
 :combat {:action-description "Activity Description"
          :event-description "Event Description"
          :outcome-party [{:name "" :dead true :damage 100}
                          {:name "" :dead false :damage 0}
                          {:name "" :dead false :damage 0}
                          {:name "" :dead false :damage 0}
                          {:name "" :dead false :damage 0}
                          {:name "" :dead false :damage 0}]
          :outcome-monsters [{:name "" :dead false :damage 0 :level 0}
                             {:name "" :dead false :damage 0 :level 0}
                             {:name "" :dead false :damage 0 :level 0}
                             {:name "" :dead false :damage 0 :level 0}
                             {:name "" :dead false :damage 0 :level 0}
                             {:name "" :dead false :damage 0 :level 0}]
          }
 :locations-visible {}
 :quests {}
 :gnomes {:list [] :next-id 1 :tavern []}}
