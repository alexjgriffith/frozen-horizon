(local item {})

(local {: items} (require :state))

;; rarity = common will generate always, 
;;          one will randomly generate until either used, or in inventory
;;          legendary will only ever be SEEN once
;;

(tset item :sprite
      {:sword {:index 1}
       :bow {:index 2}
       :staff {:index 3}
       :armour {:index 4}
       :robe {:index 5}
       :potion {:index 6}
       :map {:index 7}
       :paper {:index 8}
       :trophy {:index 9}
       :background-1 {:index 10}
       :background-2 {:index 11}
       :background-3 {:index 12}
       :legendary-sword {:index 13}
       :legendary-bow {:index 14}
       :legendary-staff {:index 15}
       :part-1 {:index 16}
       :part-2 {:index 17}
       :part-3 {:index 18}
       :missinfo {:index 19}
       :part-5 {:index 20}
       :part-6 {:index 20}
       })

(let [(iw ih w) (values (assets.sprites.items:getWidth)
                        (assets.sprites.items:getHeight)
                        48)]
  (each [name item (pairs item.sprite)]
    (local i item.index)
    (tset item :quad
          (lg.newQuad (* w (% (- i 1) 12) )
                      (* w (math.floor (/ (- i 1) 12)))
                      w w iw ih))))

(tset item :types
      {:weapon {:group :equipable :useable false :equipable true :tavern false :consumable false
                :cost-base 50 :cost-scaling 50 :cost-randomness 25}
       :armour {:group :equipable :useable false :equipable true :tavern false :consumable false
                :cost-base 50 :cost-scaling 50 :cost-randomness 25}
       :potion {:group :consumable :useable false :equipable false :tavern false :consumable true
                :cost-base 20 :cost-scaling 30 :cost-randomness 10}
       :map {:group :information :useable true :equipable false :tavern false :consumable false
             :cost-base 20 :cost-scaling 30 :cost-randomness 100}
       :quest {:group :information :useable true :equipable false :tavern false :consumable false
               :cost-base 10 :cost-scaling 10 :cost-randomness 25}
       :missinformation {:group :information :useable true :depends {:duke true} :equipable false :tavern false :consumable false
                         :cost-base 100 :cost-scaling 50 :cost-randomness 100}
       :trophy {:group :trophy :useable false :equipable false :tavern true :consumable false
                :cost-base 100 :cost-scaling 100 :cost-randomness 0}
       :part {:group :part :useable false :equipable false :tavern false :consumable false
                :cost-base 200 :cost-scaling 100 :cost-randomness 0}
       }
      )

(tset item :list (require :items))

(fn item.draw [item-id x y]
  (local i (. items.list item-id))
  ;;(lg.rectangle "fill" x y 36 36 5)
  (local background-index (if (< i.level 4) :background-1
                              (< i.level 7) :background-2
                              :background-3
                              ))
  (lg.setColor 1 1 1 1)
  (lg.draw assets.sprites.items (. item.sprite background-index :quad) x y)
  (lg.setColor i.color)
  (lg.draw assets.sprites.items (. item.sprite i.icon :quad) x y)

  )

(fn item.update [dt]
  (match (# items.inventories.hand)
    0 (tset items :hand nil)
    1 (tset items :hand (. items.inventories.hand 1))
    _ (do
        (tset items :hand (. items.inventories.hand 1))
        (tset items.inventories :hand [])
        (table.insert items.inventories.hand items.hand)))

  (when (> (# items.inventories.trash) 1)
    (local i [(. items.inventories.trash (# items.inventories.trash))])
    (tset items.inventories :trash i))
  
  )

(fn item.draw-hand [mx my]
  (when items.hand
      (item.draw items.hand (- mx 10) (- my 10))))


(fn invmap [what]
  (local map {:p1 :p :p2 :p :p3 :p :p4 :p :p5 :p :p6 :p
        :e1 :e :e2 :e :e3 :e :e4 :e :e5 :e :e6 :e
              })
  (or (. map what) what))

(fn item.valid-moves [from to]
  (match [(invmap from) (invmap to)]
    [:gnome :gnome] {:action :get}
    [:location :location] {:action :get}
    [:party :gnome] {:action :sell :when :can-sell}
    [:gnome :party] {:action :buy}
    [:party :p] {:action :consume :when :consumable}
    [:party :e] {:action :equip :when :equipable}
    [:party :trash] {:action :destroy :when :can-sell}
    [:party :use] {:action :use :when :useable}
    [:location :use] {:action :use :when :useable}
    [:party :location] {:action :put}
    [:party :party] {:action :put}
    [:party :tavern] {:action :put :when :tavern}
    [:tavern :party] {:action :get}
    [:location :party] {:action :get}
    [:e :location] {:action :unequip :when :equipable}
    [:e :party] {:action :unequip :when :equipable}
    [:location :e] {:action :equip :when :equipable}
    [:location :p] {:action :consume :when :consumable}    
    [:trash :party] {:action :get}
    _ {:action false}
    )
  )

(fn gnome-map [handle]
  (. {:p1 1 :p2 2 :p3 3 :p4 4 :p5 5 :p6 6 :e1 1
      :e2 2 :e3 3 :e4 4 :e5 5 :e6 6} handle))

(fn item.move [item from to]
  (lume.remove (. items.inventories from) item.id)
  (table.insert (. items.inventories to) item.id)
  (tset item :current-location to)
  (tset item :last-location from))

(fn item.move-to-hand [item-id]
  (local from (. items.list item-id :current-location))
  (local to :hand)
  (local i (. items.list item-id))
  (item.move i from to)
  )

(fn item.target-has-space [where]
  (< (# (. items.inventories where))
     (or (. items.limits where) 1000000000)))

(tset item :actions {})
(fn item.actions.put [item from to] true)
(fn item.actions.get [item from to] true)
(fn item.actions.sell [item from to]
  (assets.sounds.rpg.handleCoins:play)
  (local {: info} (require :state))
  (tset info :gold (+ info.gold item.sell))
  true)
(fn item.actions.buy [item from to]
  (local {: info} (require :state))
  (if (<= item.cost info.gold)
      (do (assets.sounds.rpg.handleCoins:play)
          (tset info :gold (- info.gold item.cost))
          true)
      (values false :too-poor)))
(fn item.actions.consume [item from to]
  ;; apply effect to gnome + durration
  (local {: target : value : days} item.params)
  (local value-final (math.random (. value 1) (. value 2)))
  (local days-final (when days (math.random (. days 1) (. days 2))))
  (local gnome-index (gnome-map to))
  (local party (require :party))
  (local gnome (require :gnome))
  (local gnome-id (-> gnome-index party.get-gnome-id))
  (gnome.consume gnome-id target value-final days-final)
  true)
(fn item.actions.equip [item from to]
  (local {: class : value} item.params)
  (local {: type} item)
  (local gnome-index (gnome-map to))
  (local party (require :party))
  (local gnome (require :gnome))
  (local gnome-id (-> gnome-index party.get-gnome-id))
  (local equiped (gnome.equip gnome-id type class value))
  (if equiped
      true
      (values false :wrong-class)))
(fn item.actions.unequip [item from to]
  (local {: value} item.params)
  (local {: type} item)
  (local gnome-index (gnome-map from))
  (local party (require :party))
  (local gnome (require :gnome))
  (local gnome-id (-> gnome-index party.get-gnome-id))
  (gnome.unequip gnome-id type value)
  true)

(fn item.actions.use [item from to]
  (local {: type : params} item)
  (love.event.push :use item.id type params)
  true
  )

(fn item.actions.destroy [item from to]
  (assets.sounds.rpg.doorClose_1:play)
  true)

(fn item.message [message]
  )

(fn item.error [err i from to action case]
  (db {:move-error :move-error
       : err
       :item i
       :from (or from :missing)
       :to (or to :missing)
       :action action
       :case (or case :missing-case)})
  (item.revert-position)
  (local messages (require :messages))
  (messages.error err)
  )

(fn item.move-to-slot [item-id to]
  (local from :hand)
  (db  to)
  (local last (. items.list item-id :last-location))
  (local i (. items.list item-id))
  (if (not to)
      (item.revert-position item-id)
      (let [{: action :when case} (item.valid-moves last to)
            valid-action  (not (= action false))
            valid-item-type (if case (. i case) true)
            can-sell (or i.can-sell  (= to :party))
            has-space (item.target-has-space to)
            (move-valid move-error) (do (if (and valid-action valid-item-type)
                                            ((. item.actions action) i last to)
                                            (values false :other-error)))]
        (db [can-sell valid-action valid-item-type has-space move-valid])
        (match [can-sell valid-action valid-item-type has-space move-valid]
          [false _ _ _ _] (item.error :required-item i last to action case)
          [_ false _ _ _] (item.error :invalid-action i last to action case)
          [_ _ false _ _] (item.error :invalid-item-type i last to action case)
          [_ _ _ false _] (item.error :no-space i last to action case)
          [_ _ _ _ false] (item.error move-error i last to action case)
          [true true true true true] (do (item.move i from to))
          
          ))
      )
  )

(fn item.move-by-id [item-id to?]
  (local from (. items.list item-id :current-location))
  (local to (or to? (. items.list item-id :last-location)))
  (lume.remove (. items.inventories from) item-id)
  (table.insert (. items.inventories to) item-id)
  (tset (. items.list item-id) :current-location to)
  (tset (. items.list item-id) :last-location from)
  )

(fn item.revert-position []
  (when items.hand
    (local item-id items.hand)
    (local last-location (. items.list item-id :last-location))
    (local current-location (. items.list item-id :current-location))
    (lume.remove (. items.inventories current-location) item-id)
    (table.insert (. items.inventories last-location) item-id)
    (tset (. items.list item-id) :current-location last-location)))

(fn item.get-id [where index]
  (?. items.inventories where index)
  )

(fn item.cost-from-level [level type rarity]
  "cost = base + (scaling + random) * rarity * level [1-999]"
  (local randomness (. item.types type :cost-randomness))
  (local base (. item.types type :cost-base))
  (-> (. item.types type :cost-scaling)
      (+ (* (- (/ (math.random 100000) 100000) 0) randomness))
      (* (match rarity :legendary 2 :rare 1.5 _ 1))
      (* level)
      (+ base)
      (math.floor)
      (math.max 1)
      (math.min 999)))


(fn item.new [handle where]
  (local id items.next-id)
  (local i (. item.list handle))
  (set items.next-id (+ items.next-id 1))
  (when (and where (not (= where :gnome)) (not (= where :location)))
    (table.insert (. items.inventories where) id))
  (assert i (.. "Item " handle " doesn't exist"))
  (local {: name : description : icon : type : rarity : level : cant-sell : color : params : depends} i)
  (local cost (item.cost-from-level level type rarity))
  (local sell (math.floor (* cost 0.25)))
  (local {: group : useable : equipable : tavern : consumable :depends type-depends } (. item.types type))
  (local new-item
         { : id
           : handle
           : name
           : icon
           : rarity
           : description
           : level
           :depends (or depends type-depends false)
           : group
           : useable
           : equipable
           : tavern
           : consumable
           : cost
           : sell
           :cant-sell (if cant-sell true false)
           :can-sell (if cant-sell false true)
           : type
           :color (or [1 1 1 1] color)
           : params
           :last-location :created
           :current-location (if where where :created)
           })
  (tset items.list id new-item)
  id
  )

;; how to handle rarity one?
;; thoughts - we have a list of used and party e1-6 tavern
(fn item-have [handle]
  (local {: items} (require :state))
  (local {: party : used : hand : e1 : e2 : e3 : e4 : e5 : e6 : gnome : location} items.inventories)
  (assert (= :table (type party)) :missing-inventory-party)
  (assert (= :table (type used)) :missing-inventory-used)
  (assert (= :table (type gnome)) :missing-inventory-gnome)
  (assert (= :table (type location)) :missing-inventory-location)
  (assert (= :table (type hand)) :missing-inventory-hand)
  (assert (= :table (type e1)) :missing-inventory-e1)
  (assert (= :table (type e2)) :missing-inventory-e2)
  (assert (= :table (type e3)) :missing-inventory-e3)
  (assert (= :table (type e4)) :missing-inventory-e4)
  (assert (= :table (type e5)) :missing-inventory-e5)
  (assert (= :table (type e6)) :missing-inventory-e6)
  (db (lume.concat party used gnome location hand e1 e2 e3 e4 e5 e6) :list-of-items)
  (-> (lume.concat party used gnome location hand e1 e2 e3 e4 e5 e6)
      (lume.map
       (fn [id]
         ;; (assert (= :table (type (. items.list id)))
         ;;         (.. "Missing item id:" id))
         (= handle (. items.list id :handle))))
      ((fn [x] ;;(pp [:pick x])
         (lume.any x)))
      (not)))

(fn dependency-check-location [d]
  (local {: locations-visible} (require :state))
  (or (not d.location)
      (. locations-visible d.location)))

(fn dependency-check-quest [d]
    (local {: quests} (require :state))
    (not d.quest))

(fn item-depends [i]
  (if i.depends
      (and (dependency-check-location i.depends)
           (dependency-check-quest i.depends))
      true)
  )

(fn can-pick-item [i]
  (local only-once
         (match (. i :rarity)
           :one (item-have i.handle)
           _ true))
  (local depends (item-depends i))
  (and only-once depends)
  )

(tset item :list-grouped {})
(each [t _ (pairs item.types)]
  (tset item.list-grouped t [[] [] [] [] [] [] [] [] [] []]))
(each [handle i (pairs item.list)]
  (tset i :handle handle)
  (table.insert (. item :list-grouped i.type i.level) i))

(fn item.pick-item-type [type level]
  (local maximum-sample-size 5)
  (local item-options (. item.list-grouped type))
  (local actual-level (-> level (+ (math.random 2)) (math.floor) (math.min 10) (math.max 1)))
  
  (local samples [])
  ;; count down levels
  (for [i 0 (- actual-level 1)]
    (when (< (# samples) maximum-sample-size)
      (local li (. item-options (- actual-level i)))
      (each [_ i (ipairs li)]
        (when (can-pick-item i) (table.insert samples i))
        ))
    )
  (local sample-count (# samples))
  (if (> sample-count 0)
      (. samples (math.random sample-count))
      false)
  )

(fn item.pick [level preferences? where]
  (local preferences (or preferences? {:weapon 1 :armour 1 :potion 1 :map 1 :quest 1 :missinformation 0.5 :trophy 0.1}))
  (var i (item.pick-item-type (lume.weightedchoice preferences) level))
  ;; needs a proper fix
  (if i
      (item.new i.handle where)
      false)
  )

item
