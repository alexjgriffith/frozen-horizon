(local flux (require :lib.flux))
(local {: wiggles} (require :state))

(local next-day {})
(set next-day.flux (flux.to wiggles 0.2 {:next-day 0}))

(set next-day.moves 4)
(set next-day.wiggles 3)
(fn next-day.clock []
  (next-day.flux:stop)
  (set next-day.flux
       (flux.to wiggles 0.2 {:next-day 0.1}))
  (next-day.flux:oncomplete next-day.counter-clock)
  )

(fn next-day.counter-clock []
  (next-day.flux:stop)
  (set next-day.flux
       (flux.to wiggles 0.2 {:next-day -0.1}))
  (tset next-day :wiggles (- next-day.wiggles 1))
  (if (= next-day.wiggles 0)
      (do
        (tset next-day :wiggles 3)
        (next-day.flux:oncomplete next-day.center))
      (next-day.flux:oncomplete next-day.clock))
  )

(fn next-day.center []
  (next-day.flux:stop)
  (set next-day.flux
       (flux.to wiggles 0.1 {:next-day 0}))
  (next-day.flux:oncomplete next-day.pause)
  )

(fn next-day.pause []
  (next-day.flux:stop)
  (set next-day.flux
       (flux.to wiggles 0.2 {:next-day 0}))
  (next-day.flux:delay 5)
  (next-day.flux:oncomplete next-day.clock)
  )

(fn next-day.update [dt]
  (local {: info} (require :state))
  (local out-of-moves (= info.moves 0))
  (local change (~= info.moves next-day.moves))
  (match [out-of-moves change]
    [true true] (next-day.clock)
    [false true] (do
                   (next-day.flux:stop)
                   (set next-day.flux
                        (flux.to wiggles 0.2 {:next-day 0}))))
  (set next-day.moves info.moves)
  )


(fn [dt]
  (next-day.update dt))
