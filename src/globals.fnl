(global tile-size 64)

(global js (require "lib.js"))
(global lume (require "lib.lume"))

(local cargo (require :lib.cargo))
(global assets (cargo.init
                {:dir :assets
                 :loaders {:fnl fennel.dofile}
                :processors
                {"sounds/"
                 (fn [sound _filename]
                   (sound:setVolume 0.15)
                   sound)}}))
(global object-canvases {})

(global colours (require :colours))

;; utilities
(global pt
        (fn [t]
          (assert (= :table (type t)))
          (each [key value (pairs t)]
            (print (string.format "%s\t<%s>" key (type value))))))

(global pp (fn [x] (print (fennel.view x))))

(global __message
        (fn [data name level]
          (local fennel (require :lib.fennel))
          (local debug_info (debug.getinfo 2))
          (local currentline debug_info.currentline)
          (local file (or debug_info.source ""))
          (local split-file (lume.split file "/"))
          (local short_file (. split-file (# split-file)))
          (local str (.. (fennel.view
                         {:level level
                          :location (.. short_file "+" currentline)
                          :name name
                          :data data}
                         ) "\n"))
          (match level
            :debug (if _G.debug-file
                       (love.filesystem.append _G.debug-file str)
                       (print string))
            :error (if _G.error-file
                       (_G.error-file:write str)
                       (print string))
            :log (if _G.log-file
                     (_G.log-file:write str)
                     (print string))
            )
          ))


(global ui {})
;; convert this to a macro once you've figured out how to make them global
(global db
        (fn [data name]
          (when ui.dev
            (__message data name :debug))))

(global log
        (fn [data name]
            (__message data name :log)))

(var mute false)
(var scale 1)
(tset ui :toggle-sound
        (fn []
          (if mute
              (do
                (assets.sounds.click:play)
                (do (love.audio.setVolume 1) (set mute false)))
              (do  (love.audio.setVolume 0) (set mute true)))))

(tset ui :get-mute (fn [] mute))
(tset ui :get-scale (fn [] scale))

(tset ui :web false)

(tset ui :map-edit false)
(tset ui :dev false)

(tset ui :tme
        (fn []
          (when ui.dev
            (tset ui :map-edit (not ui.map-edit)))))

(global sounds {})

;; love short-handles
(global lg love.graphics)


(local gifcat-funs {:update (fn [self dt]
                              (when self.lib
                                (self.lib.update dt)))
                    
                    :init (fn [self]
                            (when self.lib
                              (self.lib.init)))
                    
                    :close (fn [self]
                             (when self.lib
                               (self.lib.close)))
                    
                    :start (fn [self]
                             (when self.lib
                               (pp "Starting Gif Capture")
                               (local (w h) (love.window.getMode))
                               (when self.gif
                                 (self.gif:stop))
                               (tset self :gif (self.lib.newGif (.. (os.time) ".gif") w h))))
                    
                    :stop (fn [self]
                            (when (and self.lib self.gif)
                              (pp "Stoping Gif Capture")
                              (self.gif:close)
                              (tset self :gif nil)))
                    
                    :capture (fn [self]
                               (when (and self.lib self.gif)
                                 (love.graphics.captureScreenshot (fn [shot] (self.gif:frame shot)))))
                    
                    :active (fn [self]
                              (if (and self.lib self.gif) true false))
                    })

(local gifcat-mt {:__index gifcat-funs})


(global gifcat {:lib (when ui.dev (require :lib.gifcat.gifcat))
                :gif nil})

(setmetatable gifcat gifcat-mt)

;; parameters
(global text-font (lg.newFont "assets/fonts/fallingsky.otf" 16))
(global small-text-font (lg.newFont "assets/fonts/fallingsky.otf" 12))
(global button-font (lg.newFont "assets/fonts/fallingsky.otf" 18))
(global big-button-font (lg.newFont "assets/fonts/fallingsky.otf" 28))
(global title-font (lg.newFont "assets/fonts/fallingsky.otf" 60))
(global subtitle-font (lg.newFont "assets/fonts/fallingsky.otf" 40))
(global debug-font (lg.newFont "assets/fonts/inconsolata.otf" 12))
(global mono-font (lg.newFont "assets/fonts/inconsolata.otf" 14))

(global interface-cursor (love.mouse.newCursor "assets/sprites/cursor-1.png"))
(global selection-cursor (love.mouse.newCursor "assets/sprites/cursor-2.png"))
(global overworld-cursor (love.mouse.newCursor "assets/sprites/cursor-3.png"))

(love.mouse.setCursor interface-cursor)

(require :handlers)

(local (w h) (love.window.getMode))
(global window {:w (- w 1000) :h (- h 700)
                :fso (/ (- w 1000) 2) :fsy (- (/ (- h 700) 2) (if (> (- h 700) 5) 5 0))
})
