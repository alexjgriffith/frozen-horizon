{
 ;; missinformation
 :missinformation-1-1
 {
  :name "A Carefully Drafted Treatise"
  :description "Some nonsense detailing the adventures of a sky gnome in Colderado."
  :icon :missinfo
  :type :missinformation
  :rarity :common
  :level 1
  :params {:duke-vision -10}
  }

 :missinformation-1-2
 {
  :name "A Gnomes Journal"
  :description "A journal note stating that there have been no airships sited over the island recently."
  :icon :missinfo
  :type :missinformation
  :rarity :common
  :level 1
  :params {:duke-vision -10}
  }


 :missinformation-1-3
 {
  :name "A Fake Funeral"
  :description "An obituary for a sky gnome printed in Iceville."
  :icon :missinfo
  :type :missinformation
  :rarity :common
  :level 1
  :params {:duke-vision -10}
  }


 :missinformation-1-4
 {
  :name "Notes on Cuisine"
  :description "A wild story about sky gnomes not being tasty."
  :icon :missinfo
  :type :missinformation
  :rarity :common
  :level 1
  :params {:duke-vision -10}
  }


 :missinformation-1-5
 {
  :name "Flightless Wonder"
  :description "A new age gnome mantra stating there are no air gnomes."
  :icon :missinfo
  :type :missinformation
  :rarity :common
  :level 1
  :params {:duke-vision -10}
  }
 
 
  :missinformation-4-1
 {
  :name "A Sketch of a Burning Airship"
  :description "A picture of a burning airship with the walls of Frosthaven behind it."
  :icon :missinfo
  :type :missinformation
  :rarity :common
  :level 4
  :params {:duke-vision -30}
  }

 :missinformation-4-2
 {
  :name "Wild Rumours"
  :description "Some nonesense about an airship crasing into The Big Ol' Mountain."
  :icon :missinfo
  :type :missinformation
  :rarity :common
  :level 4
  :params {:duke-vision -30}
  }

 :missinformation-4-3
 {
  :name "A Sacrifice"
  :description "Some nonesense about an airship gnome being sacrificed at the Church of Our Frozen Grace."
  :icon :missinfo
  :type :missinformation
  :rarity :common
  :level 4
  :params {:duke-vision -30}
  }

 
 :missinformation-4-4
 {
  :name "Of Mice and Gnomes"
  :description "Some nonesense about an air gnome being eaten by village mice."
  :icon :missinfo
  :type :missinformation
  :rarity :common
  :level 4
  :params {:duke-vision -30}
  }

 :missinformation-4-5
 {
  :name "A Story of Trees"
  :description "A story about an air gnome that started eating trees."
  :icon :missinfo
  :type :missinformation
  :rarity :common
  :level 4
  :params {:duke-vision -30}
  }

 
  :missinformation-7-1
 {
  :name "From the Frozen Dukes own Mouth"
  :description "Some nonsense that the frozen duke doesn't even like to eat skygnomes."
  :icon :missinfo
  :type :missinformation
  :rarity :common
  :level 4
  :params {:duke-vision -50}
  }

 :missinformation-7-2
 {
  :name "Tales of Tragedy"
  :description "A story about the air gnome being eaten by Frost Giants in the Wilderness."
  :icon :missinfo
  :type :missinformation
  :rarity :common
  :level 4
  :params {:duke-vision -50}
  }

  :missinformation-7-3
 {
  :name "Tales of Escape"
  :description "A story about the air gnome escaping the island in their airship."
  :icon :missinfo
  :type :missinformation
  :rarity :common
  :level 4
  :params {:duke-vision -50}
  }
 }
