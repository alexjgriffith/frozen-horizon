(local gamestate (require :lib.gamestate))
(local stdio (require :lib.stdio))
(local flux (require :lib.flux))
(local wiggles (require :wiggles))
(local fade (require :lib.fade))
(local state (require :state))

(tset state :mode (if p720 :mode-opening :mode-story))

(fn setup []
  (require :tests)
  (match true
    true (do
           (tset state :mode :mode-map)
           (ui.toggle-sound)
           (local (_w _h params) (love.window.getMode))
           (tset params :x 0)
           (tset params :y 0)
           (love.window.setMode _w _h params)
           (tset _G :debug-file "frozen-horizon-debug")
           ;; (_G.debug-file:write "; Frozen Horizon Debug Log")
           (love.filesystem.write "frozen-horizon-debug" "; Frozen Horizon Debug Log\n")
           (local fennel (require :lib.fennel))
           (db  "/tmp/frozen-horizon-debug" :debug-file)
           (print :DEVMODE)
           (db params :getModeParams)
           (stdio.start))))

(fn love.load [args]
  ;; (love.graphics.setDefaultFilter "nearest" "nearest" 0)
  (when (= (. args 1) :dev) (tset ui :dev true))
  (when ui.dev (setup))
  (fade.set-colour colours.black)
  (if (= :web (. args 1))
      (tset ui :web true)
      (tset ui :web false))
  (when (and (not ui.web) (= (love.system.getOS) :Linux)) (tset ui :dev true))
  (tset sounds :bgm (if ui.web
                 (love.audio.newSource "assets/music/winter.ogg" "static")
                 (. _G.assets.music "winter")))

  (tset sounds :birds (if ui.web
                 (love.audio.newSource "assets/music/birds.ogg" "static")
                 (. _G.assets.music "birds")))

  (tset sounds :fire (if ui.web
                        (love.audio.newSource "assets/music/fire.ogg" "static")
                        (. _G.assets.music "fire")))

  (tset sounds :wind (if ui.web
                        (love.audio.newSource "assets/sounds/wind.ogg" "static")
                        (. _G.assets.sounds "wind")))

  
  (assets.sounds.page:play)
  
  (sounds.bgm:setLooping true)
  (sounds.bgm:setVolume 0.2)
  ;; (sounds.bgm:play)

  (sounds.wind:setLooping true)
  (sounds.wind:setVolume 0.05)
  (sounds.wind:play)
  
  (sounds.birds:setLooping true)
  (sounds.birds:setVolume 0.1)
  (sounds.birds:play)
  
  (sounds.fire:setLooping true)
  (sounds.fire:setVolume 0.25)
  
  (gamestate.registerEvents)
  (gamestate.switch (require state.mode))
  (gifcat:init))

(fn love.draw []
  (lg.translate window.fso window.fsy))

(fn love.quit []
  (gifcat:close))

(fn love.update [dt]
  (local {: hover : items : info} (require :state))
  (when (not hover.active)
    (when hover.previous
      (let [interface (require :interface)]
        ;; (pp :close-popup)
        ;; (interface.popup:close)
        (love.mouse.setCursor interface-cursor)
        (tset info :hover-item false)
        ))
    (tset hover :previous false))
  (tset hover :active false)
  (flux.update dt)
  (wiggles dt)
  (gifcat:update dt)
  (when (and (not (love.mouse.isDown 1)) items.hand)
    (local item (require :item))
    (item.move-to-slot items.hand items.over)
    ;;(pp items.over)
    )
  (fade.update (math.min dt 0.032))
  (tset items :over false)
  
  )

(fn love.mousepressed [])

(fn love.threaderror [thread errorstr]
  (pp (.. "Thread eror!\n" errorstr)))

(fn love.mousereleased [button]
  (local {: hover} (require :state))
  (tset hover :click-down false))

(fn love.keypressed [key code repeat]
  (local utilities (require :utilities))
  ;; (pp key)
  (when (not repeat)
    (match key
      :r (if (gifcat:active) (gifcat:stop) (gifcat:start))
      :p (when (not ui.web) (love.graphics.captureScreenshot :screenshot.png))
      :0 (let [item (require :item)]
           (item.new :sail :party)
           (item.new :mast :party)
           (item.new :lightener :party))
      :- (utilities.toggle-easy-mode)
      )))
