{ ;; armour

  :thick-coat
 {
  :name "A Thick Coat"
  :description "This thick coat will keep you safe from most bumps and bruises."
  :icon :armour
  :type :armour
  :rarity :common
  :level 1
  :params {:class [:warrior :archer] :value 1}
  }
  
 :padded-jacket
 {
  :name "A Padded Jacket"
  :description "While this will keep you warm its protection from swords is overhyped."
  :icon :armour
  :type :armour
  :rarity :common
  :level 2
  :params {:class [:warrior :archer] :value 2}
  }

 :studded-armour
 {
  :name "A Studded Jacket"
  :description "This Jacket is certainly better suited to stopping swords than keeping you warm."
  :icon :armour
  :type :armour
  :rarity :common
  :level 4
  :params {:class [:warrior :archer] :value 4}
  }

 :plate-armour
 {
  :name "Plate Armour"
  :description "In this armour you are in business."
  :icon :armour
  :type :armour
  :rarity :common
  :level 7
  :params {:class [:warrior :archer] :value 7}
  }

  
  :long-robe
 {
  :name "A Long Robe"
  :description "This robe is nice and long."
  :icon :robe
  :type :armour
  :rarity :common
  :level 1
  :params {:class [:mage] :value 1}
  }

  :mystic-robe
 {
  :name "A Mystical Robe"
  :description "This robe gives off a mystical aura."
  :icon :robe
  :type :armour
  :rarity :common
  :level 4
  :params {:class [:mage] :value 2}
  }

}
