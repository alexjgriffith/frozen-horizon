(local fade (require :lib.fade))

(fn update [gs dt])


(local story "Your airship has crashed on a glacial island far out at sea. Gather your gnomic breatheren and rummage through the island to find the missing parts to fix your airship.

Beware, there is a gnome thirsty duke who lives on the island. His soul is frozen to the core by his lust to eat airborne gnomes. Don't let him capture you.
")

(fn enter [gs]
  (fade.in)
  (assets.sounds.page:play)
  (assets.music.fire:play)
  (assets.music.birds:stop))

(local flux (require :lib.flux))
(local ship {:y 100 :alpha 0 :y2 0})
(flux.to ship 10 {:y 600})
(flux.to ship 15 {:y2 600})
(local alpha-shift (flux.to ship 2 {:alpha 1}))
(alpha-shift:delay 4)

(fn set-alpha [[r g b] a]
  (lg.setColor r g b a)
  )

(local story-canvas (lg.newCanvas 1280 720))

(local cover-canvas (lg.newCanvas 1280 1200))

(fn draw [gs]
  (lg.clear colours.white)
  
  (lg.push :all)
  (lg.origin)
  (lg.setCanvas cover-canvas)
  (lg.clear 0 0 0 0)
  (var cover-a 1)
  (for [i 0 1200]
    (lg.setColor 1 1 1 cover-a)
    (set cover-a (- cover-a 0.005))
    (lg.rectangle :fill 0 i 1280 2))
  (lg.setCanvas)
  (lg.pop)

  (lg.setColor 1 1 1 1)
  ;; (lg.draw cover-canvas)
  
  (lg.push :all )
  (lg.origin)
  (lg.setCanvas story-canvas)
  (lg.clear 0 0 0 0)
  (lg.translate 0 120)
  (lg.setColor 1 1 1 1)
  (lg.setFont subtitle-font)
  (lg.printf story 20 0 960 :left)
  (lg.setColor 1 1 1 1)
  (lg.setBlendMode :multiply :premultiplied)
  (lg.translate 0 (- ship.y2 200 ))
  (lg.draw cover-canvas)
  (lg.setCanvas)
  (lg.pop)

  (lg.setColor 1 1 1 1)
  (lg.push)
  (lg.translate 0 ship.y)
  (lg.draw assets.sprites.crash)
  (lg.pop)
  (lg.setColor colours.brown)
  (lg.draw story-canvas)
  (lg.translate 0 20)
  (lg.setFont title-font)
  
  (lg.push)
  (lg.printf "Frozen Horizon" 0 0 1000 :center)
  (lg.translate 0 100)
  (lg.translate 300 450)
  
  (local (w h) (values 400 55))
  (var (forcolour backcolour) (values colours.brown colours.white))
  (local (mx my) (love.mouse.getPosition))
  (local (screen-mx screen-my) (lg.inverseTransformPoint mx my))
  (local hover (_G.pointWithin screen-mx screen-my 0 0 w h))
  (when hover
    (set (forcolour backcolour) (values colours.white colours.brown))
    (love.event.push :hover :continue)
    (when (and hover (love.mouse.isDown 1))
      (love.event.push :click :continue)))
  (set-alpha backcolour ship.alpha)
  (lg.rectangle :fill 0 0 w h)
  (set-alpha forcolour ship.alpha)
  (lg.rectangle :line 0 0 w h)
  (lg.setFont subtitle-font)
  (lg.printf "Play" 0 0 w :center)
  (lg.pop)
  (lg.translate 0 630)
  (set-alpha colours.brown ship.alpha)
  (lg.setFont button-font)
  (lg.printf "Game By: AlexJGriffith" 20 0 800 :left)
  (lg.printf "Music By: Cyber SDF" 0 0 980 :right)
  
  (lg.origin)
  (lg.setColor 1 1 1 1)
  (lg.draw fade.canvas)
)

{: draw : update : enter}
