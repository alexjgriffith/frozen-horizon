(local weapons (require :weapons)) ;; done
(local armour (require :armour)) ;; done
(local potions (require :potions)) ;; done
(local quests (require :quests)) ;; done
(local maps (require :maps)) ;; done
(local missinformation (require :missinformation)) ;; done
(local trophies (require :trophies)) ;; done
(local parts (require :parts)) ;; done

(local ret  (lume.merge weapons armour potions quests maps missinformation trophies parts))

ret
