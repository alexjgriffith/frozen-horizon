(local icons {})
(tset icons :icons
      {:mountain-1   {:x 1  :y 1 :w 2 :h 2 :ox 0  :oy -24}
       :hideout      {:x 3  :y 1 :w 2 :h 2 :ox 0  :oy -24}
       :mountain-2   {:x 5  :y 1 :w 2 :h 2 :ox 0  :oy -24}
       :mountain-3   {:x 7  :y 1 :w 2 :h 2 :ox 0  :oy -24}
       :church       {:x 9  :y 1 :w 1 :h 1 :ox 24 :oy 12}
       :tavern       {:x 9  :y 2 :w 1 :h 1 :ox 24 :oy 12}
       :fancy-tower  {:x 10 :y 1 :w 1 :h 2 :ox 24 :oy -24}
       :fortress     {:x 11 :y 1 :w 2 :h 1 :ox 0  :oy 12}
       :tower        {:x 11 :y 2 :w 1 :h 1 :ox 24 :oy 12}
       :hills        {:x 12 :y 2 :w 1 :h 1 :ox 24 :oy 12}
       :house-tall-r {:x 13 :y 1 :w 1 :h 1 :ox 24 :oy 12}
       :house-tall-l {:x 13 :y 2 :w 1 :h 1 :ox 24 :oy 12}
       :house-long-r {:x 14 :y 1 :w 1 :h 1 :ox 24 :oy 12}
       :house-long-l {:x 14 :y 2 :w 1 :h 1 :ox 24 :oy 12}
       :house-wide-r {:x 15 :y 1 :w 1 :h 1 :ox 24 :oy 12}
       :house-wide-l {:x 15 :y 2 :w 1 :h 1 :ox 24 :oy 12}
       :house-r      {:x 16 :y 1 :w 1 :h 1 :ox 24 :oy 12}
       :house-l      {:x 16 :y 2 :w 1 :h 1 :ox 24 :oy 12}
       :tree-1       {:x 17 :y 1 :w 1 :h 1 :ox 0  :oy 0}
       :tree-2       {:x 17 :y 2 :w 1 :h 1 :ox 0  :oy 0}
       :gnome-1      {:x 18 :y 1 :w 1 :h 1 :ox 0  :oy 0}
       :gnome-2      {:x 18 :y 2 :w 1 :h 1 :ox 0  :oy 0}
       :gnome-3      {:x 19 :y 2 :w 1 :h 1 :ox 0  :oy 0}
       :village      {:x 1  :y 4 :w 1 :h 1 :ox 24 :oy 12}
       :town-1       {:x 2  :y 4 :w 1 :h 1 :ox 24 :oy 12}
       :town-2       {:x 3  :y 4 :w 1 :h 1 :ox 24 :oy 12}
       :city-1       {:x 4  :y 4 :w 2 :h 1 :ox 0  :oy 12}
       :city-2       {:x 6  :y 3 :w 2 :h 2 :ox 0  :oy -36}
       :airship      {:x 8  :y 4 :w 2 :h 1 :ox 0  :oy 12}
       :stand-1      {:x 10 :y 3 :w 2 :h 2 :ox 0  :oy 0}
       :stand-2      {:x 12 :y 3 :w 2 :h 2 :ox 0  :oy 0}
       :stand-3      {:x 14 :y 3 :w 2 :h 2 :ox 0  :oy 0}
       :stand-4      {:x 16 :y 3 :w 2 :h 2 :ox 0  :oy 0}
       :eye-closed   {:x 18 :y 3 :w 2 :h 1 :ox 0  :oy 0}
       :eye-open     {:x 18 :y 4 :w 2 :h 1 :ox 0  :oy 0}
       })

(let [(iw ih w) (values (assets.sprites.map-icons:getWidth)
                        (assets.sprites.map-icons:getHeight)
                        48)]
  (each [index icon (pairs icons.icons)]
    (tset icon :quad
          (lg.newQuad (* w (- icon.x 1))
                      (* w (- icon.y 1))
                      (* w icon.w)
                      (* w icon.h)
                      iw
                      ih
                      ))))

(fn icons.draw [which x y]
       (local icon (. icons.icons which))
       (lg.draw assets.sprites.map-icons icon.quad (+ (or x 0) icon.ox ) (+ (or y 0) icon.oy)))

icons
