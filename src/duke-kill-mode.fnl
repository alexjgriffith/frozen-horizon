(local fade (require :lib.fade))

(fn update [gs dt])

(fn enter [gs]
  
  (fade.in))

(fn draw [gs]
  (lg.clear colours.black)
  (lg.setColor colours.white)
  (lg.translate 0 200)
  (lg.setFont title-font)
  (lg.printf "Game Over" 0 0 1000 :center)
  (lg.translate 0 100)
  (lg.setFont subtitle-font)
  (lg.printf "You were captured by the Frozen Duke before you could escape the island\n\nBetter luck next crash landing" 100 0 800 :center)
  (lg.translate 300 250)
  (local (w h) (values 400 55))
  (var (forcolour backcolour) (values colours.white [1 1 1 0]))
  (local (mx my) (love.mouse.getPosition))
  (local (screen-mx screen-my) (lg.inverseTransformPoint mx my))
  (local hover (_G.pointWithin screen-mx screen-my 0 0 w h))
  (when hover
    (set (forcolour backcolour) (values colours.black colours.white))
    (love.event.push :hover :quit)
    (when (and hover (love.mouse.isDown 1))
      (love.event.push :click :quit)))
  (lg.setColor backcolour)
  (lg.rectangle :fill 0 0 w h)
  (lg.setColor forcolour)
  (lg.rectangle :line 0 0 w h)
  (lg.printf "Quit" 0 0 w :center)
  (lg.origin)
  (lg.draw fade.canvas))

{: update : draw : enter}
