(local hex {})

(fn hex.ij-to-xy [i j w]
  (local h (math.sqrt (- (^ w 2) (^ (/ w 2) 2))))
  (local iseven (% i 2))
  (values
   (* 1.5 w (- i 0))
   (+ (* 2 h (- j 0)) (* iseven h))))

(fn hex.xy [i j r w]
  (local pb3 (/ math.pi 3))
  (local v [])
  (local (x y) (hex.ij-to-xy i j w))
  (for [i 1 6]
    (local xp (+ x (* r  (math.cos (* i pb3)))))
    (local yp (+ y (* r  (math.sin (* i pb3)))))
    (table.insert v xp)
    (table.insert v yp)
    )
  v
  )

(fn hex.get-neighbours [i j w]
  (local up (- (% (+ i 1) 2)))
  (local down (% (+ i 0) 2))
  (local array [[0 -1] [1 up] [1 down] [0 1] [-1 down] [-1 up]])
  (icollect [_ [ia ja] (ipairs array)] (hex.ij-to-index (+ i ia) (+ j ja) w)))


(fn hex.point-within [h x y]
  (var (neg pos) (values 0 0))
  (for [i 1 6]
    (local (x1 y1) (values (. h (-> i (- 1) (% 6) (* 2) (+ 0) (+ 1)))
                           (. h (-> i (- 1) (% 6) (* 2) (+ 1) (+ 1)))
                           ))
    (local (x2 y2) (values (. h (-> i (- 0) (% 6) (* 2) (+ 0) (+ 1)))
                           (. h (-> i (- 0) (% 6) (* 2) (+ 1) (+ 1)))))
    ;; cross product
    (var cross (- (* (- x x1) (- y2 y1))
                  (* (- y y1) (- x2 x1))))
    (when (> cross 0) (set pos (+ pos 1)))
    (when (< cross 0) (set neg (+ neg 1)))
    )
  (if (and (> pos 0) (> neg 0))
      false
      true)
  )

(fn hex.get-bounds [i j w]
  (local (x y) (hex.ij-to-xy i j w))
  [(- x w) (- y w) (* 2 w) (* 2 w)]
  )

(fn hex.index-to-ij [index w]
  (values  (math.floor
            (/ (- index 1) (+ w 1)))
           (% (- index 1) (+ w 1))))

(fn hex.ij-to-index [i j w]
  (+ (* i (+ w 1)) j 1))

hex
