(local icons (require :icons))

(local locations {})

(tset locations :list
      {:mountain-1   {:icon :mountain-1  :type :mountain    :name "The Big Ol' Mountain" :actions [] :level 4
                      :information "The Big Ol' Mountain looms above your party. You can't make out its snowy peak in the clouds. Looking around you see hoof prints."}
       
       :hideout      {:icon :hideout     :type :mountain    :name "Smuggler's Notch" :actions [] :level 5
                      :information "Your party stares in awe at the jagged crags cut through the snow reaching for the sky. Looking closely they can see a house carved into the size of the mountain. An irresistible mystery to a gnome."}
       
       :mountain-2   {:icon :mountain-2  :type :mountain    :name "The Ice Cliff" :actions [] :level 4
                      :information "Your party slides up to the edge of The Ice Cliff. Shivering they carefully step back."}
       
       :mountain-3   {:icon :mountain-2  :type :mountain    :name "Lil Steepy" :actions [] :level 4
                      :information "Your party arrives at Lil Steepy. Named due to its massive size and gradual slopes of course."}
       
       :church       {:icon :church      :type :church      :name "Our Frozen Grace" :actions [] :level 6
                      :information "Your party arrives at Our Frozen Grace. The icy steeple looms overhead. They can hear the sounds of gnome monks chanting inside."}
       
       :tavern       {:icon :tavern      :type :tavern      :name "The Friggin Cold Tavern" :actions [] :level 1
                      :information "The warm fires at The Friggin Cold Tavern draw in your party. They rest their bones from their weary adventure and look to find new adventurers to join their ranks."}
       
       :fancy-tower  {:icon :fancy-tower :type :magetower   :name "The Mage's Lookout" :actions [] :level 7
                      :information "Who knew towers could be so tall and so magical? Your party stares in awe at the towering spires feeling the icy magical presence emanate forth."}
       
       :fortress     {:icon :fortress    :type :frozenduke  :name "The Dukes Haunt" :actions [] :level 8
                      :information "The blood of your gnome party runs cold with fear. This fortress is the haunt of the starving Frozen Duke. Being this close draws his gaze stronger than ever."}
       
       :tower        {:icon :tower       :type :tower       :name "Frozen Fortress" :actions [] :level 4
                      :information "Your party arrives at the Frozen Fortress. The cool walls of its towering tower stills the beating heart of each party member."}
       
       :hills        {:icon :hills       :type :mountain    :name "The Slippery Hills" :actions [] :level 3
                      :information "Your party arrives at the Slippery Hills. They look around carefully, it feels like something or someone is watching them."}
       
       :tree-1       {:icon :tree-1      :type :tree        :name "The Lonely Aspen" :actions [] :level 1
                      :information "Out in the frozen tundra stands a lonely aspen. How it survives in this hellishly cold climate is the best gnome's guess. You empathize with its plight."}
       
       :tree-2       {:icon :tree-1      :type :tree        :name "The Lonely Fir" :actions [] :level 1
                      :information "Your party arrives a The Lonely Fir. There is a little burrow dug into its roots, the perfect size for a little gnome nap."}
       
       :village      {:icon :village     :type :village     :name "The Cozy Village" :actions [] :level 1
                      :information "Your party wanders into The Cozy Village. Thin trails of steam rise from the neat row of houses and a faint smell of baking bread is in the air."}
       
       :town-1       {:icon :town-1      :type :town        :name "Iceville" :actions [] :level 3
                      :information "Your party walks down the main road in Iceville. The local gnomes wave pleasantly and the warm fire of the local legend, The Badger Inn, beckons your travel worn party."}
       
       :town-2       {:icon :town-2      :type :town        :name "Subzero" :actions [] :level 3
                      :information "Your party sets up on the outskirts of Subzero. They can hear the hammers of the blacksmith carrying across the frigid air and see the wall of the famous Gnomic Convent in the distance."}
       
       :city-1       {:icon :city-1      :type :city        :name "Frosthaven" :actions [] :level 5
                      :information "Your party wanders down the bustling streets of Frosthaven. They shiver as they look around at the sights in awe at the size and scale of everything."}
       
       :city-2       {:icon :city-2      :type :city        :name "Colderado" :actions [] :level 5
                      :information "Your party arrives at Colderado. An eerie wailing can be heard in the air. It's cold."}
       
       :airship      {:icon :airship     :type :airship     :name "Your Airship" :actions [] :level 1
                      :information "Your party arrives at the wreckage of your airship. It looks to be mostly in tact. It just needs some new sails, a new mast and some lightening fluid."}
       
       :stand-1      {:icon :stand-1     :type :tree-stand  :name "The Aspen Stand" :actions [] :level 2
                      :information "Your party arrives at the Aspen Stand. The leaves blow gently in the cold winter breeze. Looking around you can see the traces of a bandit camp."}
       
       :stand-2      {:icon :stand-2     :type :tree-stand  :name "The Fir Stand" :actions [] :level 2
                      :information "Your party arrives at The Fir Stand. The leaves blow gently in the cold winter breeze. Looking around you can see the traces of a bandit camp."}
       
       :stand-3      {:icon :stand-3     :type :tree-stand  :name "The Aspen Stand" :actions [] :level 2
                      :information "Your party arrives at the Aspen Stand. The leaves blow gently in the cold winter breeze. Looking around you can see the traces of a bandit camp."}
       
       :stand-4      {:icon :stand-4     :type :tree-stand  :name "The Fir Stand" :actions [] :level 2
                      :information "Your party arrives at The Fir Stand. The leaves blow gently in the cold winter breeze. Looking around you can see the traces of a bandit camp."}
       :wilderness {:icon :stand-4 :type :wild :name  "The Wilderness" :actions [] :level 4
                    :information "Your party is out in the wilderness. The wolves are howling in the distance. It's cold."}
       })

(tset locations :types
      {:mountain   {:actions [:search-wild :rest-wild :delve]}
       :church     {:actions [:search-town :rest-town :listen-to-music]}
       :tavern     {:actions [:test-message :test-get-items :test-combat :test-rest]}
       :magetower  {:actions [:search-town :rest-town :chat-up-mage]}
       :frozenduke {:actions [:search-town :rest-town :hide-from-duke]}
       :tower      {:actions [:search-town :rest-town :visit-armoury]}
       :tree       {:actions [:search-wild :rest-wild :frolic]}
       :village    {:actions [:search-town :rest-town :get-directions]}
       :town       {:actions [:search-town :rest-town :visit-armoury]}
       :city       {:actions [:search-town :rest-town :go-to-library]}
       :airship    {:actions [:search-wild :rest-wild :explore-wreckage]}
       :tree-stand {:actions [:search-wild :rest-wild :frolic]}
       :wild {:actions [:search-wild :rest-wild]}
       })

(fn locations.get-actions [location-name]
  (local l (or (. locations.list location-name)
               (. locations.list :wilderness)))
  (local t (. locations.types l.type))
  t.actions
  )

(fn locations.get-details [location-name]
  (local l (or (. locations.list location-name)
               (. locations.list :wilderness)))
  (local t (. locations.types l.type))
  {:type l.type :name l.name :icon l.icon :information l.information
   :actions t.actions :level l.level :handle location-name})

locations
