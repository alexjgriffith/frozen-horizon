(local fade (require :lib.fade))
(fn update [gs dt])

(local flux (require :lib.flux))
(local ship {:x 1000})
;;(local ship-flux (flux.to ship 1 {:x 200}))

(local ship-flux (flux.to ship 1 {:x 200}))
(ship-flux:ease :linear)

(fn enter [gs]
  (fade.in))

(fn draw [gs]  
  (lg.push :all)
  
  (lg.clear colours.white)
  
  (lg.setColor colours.brown)
  (lg.translate 0 20)
  (lg.setFont title-font)
  (lg.printf "Frozen Horizon" 0 0 1000 :center)
  (lg.translate 0 100)
  (lg.setColor [1 1 1 1])

  (lg.push)
  (lg.translate ship.x 0)
  (lg.draw assets.sprites.skyship 0 0)
  (lg.pop)

  (lg.push)
  (lg.translate 300 400)
  (local (w h) (values 400 55))
  (var (forcolour backcolour) (values colours.brown [1 1 1 0]))
  (local (mx my) (love.mouse.getPosition))
  (local (screen-mx screen-my) (lg.inverseTransformPoint mx my))
  (local hover (_G.pointWithin screen-mx screen-my 0 0 w h))
  (when hover
    (set (forcolour backcolour) (values colours.white colours.brown))
    (love.event.push :hover :play)
    (when (and hover (love.mouse.isDown 1))
      (love.event.push :click :play)))
  (lg.setColor backcolour)
  (lg.rectangle :fill 0 0 w h)
  (lg.setColor forcolour)
  (lg.rectangle :line 0 0 w h)
  (lg.setFont subtitle-font)
  (lg.printf "Play" 0 0 w :center)
  (lg.pop)
  (lg.push :all)
  (lg.setColor colours.brown)
  (lg.translate 0 500)
  (lg.setFont subtitle-font)
  (lg.printf "Game By: AlexJGriffith" 20 0 800 :left)
  (lg.printf "Music By: Cyber SDF" 0 0 980 :right)
  (lg.pop)
  (lg.translate 300 250)
  (lg.origin)
  (lg.setColor 1 1 1 1)
  (lg.draw fade.canvas)
  (lg.pop)

)


{: draw : update : enter}
