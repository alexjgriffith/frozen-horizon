(local fennel (require :lib.fennel))

(local actions (. (require :actions) :list))
(local items (require :items))
(local events (. (require :events) :list))
(local location-types (. (require :locations) :types))
(local locations (. (require :locations) :list))

;; Make sure all events referenced by actions exist
(do
  (var events-in-actions [])
  (each [_ action (pairs actions)]
    (each [event-name probability (pairs action.events)]
      (table.insert events-in-actions event-name)))
  (var missing-events [])
  (each [_ event-name (ipairs events-in-actions)]
    (local e (. events event-name))
    (when (not e)
      (table.insert missing-events event-name)))
  (assert (= (# missing-events) 0 ) (.. "Missing the following events " (fennel.view missing-events)))
  )

;; Make sure all actions referenced by location types exist
(do
  (var actions-in-locations [])
  (each [_ location (pairs location-types)]
    (each [_ action-name (ipairs location.actions)]
      (table.insert actions-in-locations action-name)))
  (var missing-actions [])
  (each [_ action-name (ipairs actions-in-locations)]
    (local a (. actions action-name))
    (when (not a)
      (table.insert missing-actions action-name)))
  (assert (= (# missing-actions) 0 ) (.. "Missing the following actions " (fennel.view missing-actions)))
  )


;; Make sure all actions and locations referenced by quests exist
(do
  (var actions-in-quests [])
  (var locations-in-quests [])
  (each [_ item (pairs items)]
    (when (= :quest item.type)
      (table.insert actions-in-quests item.params.action)
      (table.insert locations-in-quests item.params.location)))
  (var missing-actions [])
  (each [_ action-name (ipairs actions-in-quests)]
    (local a (. actions action-name))
    (when (not a)
      (table.insert missing-actions action-name)))
  (assert (= (# missing-actions) 0 ) (.. "Missing the following actions " (fennel.view missing-actions)))
  (var missing-locations [])
  (each [_ location-name (ipairs locations-in-quests)]
    (local l (. locations location-name))
    (when (not l)
      (table.insert missing-locations location-name)))
  (assert (= (# missing-locations) 0 ) (.. "Missing the following locations " (fennel.view missing-locations)))
  )

;; Make sure all locations referenced by maps exist
(do

  (var locations-in-quests [])
  (each [_ item (pairs items)]
    (when (= :map item.type)
      (table.insert locations-in-quests item.params.location)))
  (var missing-locations [])
  (each [_ location-name (ipairs locations-in-quests)]
    (local l (. locations location-name))
    (when (not l)
      (table.insert missing-locations location-name)))
  (assert (= (# missing-locations) 0 ) (.. "Missing the following locations " (fennel.view missing-locations)))
  )


;; Make sure all items referenced by events exist
(do
  (var items-in-events [])
  (each [_event-name event (pairs events)]
    (when event.item
      (table.insert items-in-events event.item)))
  (var missing-items [])
  (each [_ item-name (ipairs items-in-events)]
    (local i (. items item-name))
    (when (not i)
      (table.insert missing-items item-name)))
  (assert (= (# missing-items) 0 ) (.. "Missing the following items " (fennel.view missing-items)))
  )
