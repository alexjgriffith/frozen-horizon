(local {: combat} (require :state))

(local logic {})

(local monsters {})
(tset monsters :list
      {:wolf {:name "Wolf" :fight-value 0.3 :attack 0.3 :defense 0.3 :health 10}
       :coyote {:name "Coyote" :fight-value 0.2 :attack 0.3 :defense 0.2 :health 10}
       :bear {:name "Bear" :fight-value 0.2 :attack 0.3 :defense 0.5 :health 20}
       :bandit {:name "Bandit" :fight-value 0.2 :attack 0.6 :defense 0.2 :health 10}       
       :giant {:name "Frost Giant" :fight-value 0.2 :attack 0.6 :defense 0.2 :health 10}
       :wear {:name "Weregnome" :fight-value 0.2 :attack 0.6 :defense 0.2 :health 10}
       :drunk {:name "Drunk Gnome" :fight-value 0.2 :attack 0.4 :defense 0.2 :health 10}
       :hunter {:name "Cannibal" :fight-value 0.5 :attack 0.4 :defense 0.2 :health 5}
       :mage {:name "Mage" :fight-value 0.5 :attack 1 :defense 0.2 :health 5}
       :preacher {:name "Preacher" :fight-value 0.2 :attack 0.4 :defense 0.8 :health 10}
       })

(tset monsters :locations
      {:wild {:wolf 1 :coyote 1 :bear 0.3 :bandit 0.2 :giant 0.1}
       :town {:drunk 1 :hunter 1 :wear 0.2}
       :church {:drunk 1 :preacher 2 :hunter 1 :wear 0.2}
       :mage {:drunk 1 :mage 2 :hunter 1 :wear 0.2}})

(fn monsters.location-mapping [location-name]
  (local location-mapping
         {:tavern :town
          :magetower :mage
          :tower :town
          :village :town
          :city :town
          :church :church})
  (or (. location-mapping location-name)
      :wild))

(fn monsters.stat-from-level [name level stat max]
    (->
     (. monsters :list name stat)
     (* level)
     (+ (match stat
          :health 10
          _ 1))
     (math.floor)
     (math.min (or max 10))
     (math.max 1)))

(fn choose-monster [location]
  (local options (. monsters.locations (monsters.location-mapping location)))
  (assert options (.. "Location " location " not mapped in monster mapping."))
  (lume.weightedchoice options))

(fn monsters.pick [level location]
  (local {:info {: fight-value-reduction}} (require :state))
  (local monster-name (choose-monster location))
  {:fight-value (monsters.stat-from-level monster-name level :fight-value (- 5 fight-value-reduction))
   :defense (monsters.stat-from-level monster-name level :defense)
   :attack (monsters.stat-from-level monster-name level :attack)
   :health (monsters.stat-from-level monster-name level :health 200)
   :name (. monsters.list monster-name :name)
   :level (math.random (math.max 1 (- level 1)) (math.min 9 (+ level 1)))
   : location
   :handle monster-name
   }
  )

(fn monsters.make-party [count level location]
  (local monster-party [])
  (for [i 1 count]
    (table.insert monster-party (monsters.pick level location)))
  monster-party)

(fn logic.zero []
  (tset combat :action-description "")
  (tset combat :event-description "")
  (each [_ p (ipairs combat.outcome-party)]
    (tset p :name "")
    (tset p :damage 0)
    (tset p :dead false)
    (tset p :level 0)
    (tset p :health 0)
    )
  (each [_ p (ipairs combat.outcome-monsters)]
    (tset p :name "")
    (tset p :damage 0)
    (tset p :dead false)
    (tset p :level 0)
    (tset p :health 0)
    )  
  )

(fn fight-determination [gnome monster]
  (local gnome-fight (+ (/ (math.random 20) 20)
                        (* 0.2 (+ gnome.fight-value
                                  gnome.fight-value-bonus
                                  gnome.potion-fight-value-bonus))))
  (local monster-fight (+ (/ (math.random 20) 20) (* 0.2 monster.fight-value)))
  (if (>= gnome-fight monster-fight)
      :gnome
      :monster))

(fn damage-determination [attacker defender]
  (local attack-value
         (+ attacker.attack
            (or attacker.attack-bonus 0)
            (or attacker.potion-attack-bonus 0)))
  (local defense-value
         (+ defender.defense
            (or defender.defense-bonus 0)
            (or defender.potion-defense-bonus 0)
            (or defender.defense-camp-bonus 0)))
  (if (> attacker.health 0)
      (math.floor (+ attack-value (math.random 20) (- defense-value)))
      0)
  )

(fn logic.fight [count level location event-description action-description]
  (logic.zero)
  (tset combat :action-description action-description)
  (tset combat :event-description event-description)
  (local party (require :party))
  (local monster-list (monsters.make-party count level location))
  (local party-list (party.get-members))
  (local largest (math.max (# party-list) (# monster-list)))
  (each [i m (ipairs monster-list)]
    (local cm (. combat.outcome-monsters i))
    (tset cm :name m.name)
    (tset cm :level m.level)
    (tset cm :health m.health)
    )
  (each [i p (ipairs party-list)]
    (tset (. combat.outcome-party i) :name p.name)
    ;; (tset (. combat.outcome-party i) :level p.level)
    )
  (for [i 1 5] ;; fight for 5 rounds
    (for [j 1 largest]
      (local monster-index (+ 1 (% (- j 1) (# monster-list))))
      (local party-index (+ 1 (% (- j 1) (# party-list))))
      (local monster (. monster-list monster-index))
      (local monster-record (. combat.outcome-monsters monster-index))
      (local gnome (. party-list party-index))
      (local gnome-record (. combat.outcome-party party-index))
      (local winner (fight-determination gnome monster))
      (match winner
        :monster (let [damage (damage-determination monster gnome)]
                   (tset  gnome :health (- gnome.health damage))
                   (tset gnome-record :damage (-> (+ damage gnome-record.damage) (math.max 0)))
                   (when (<= gnome.health 0) (tset gnome-record :dead true))
                   )
        :gnome (let [damage (damage-determination gnome monster)]
                 (tset  monster :health (- monster.health damage))
                 (tset monster-record :damage (-> (+ damage monster-record.damage) (math.max 0)))
                 (when (<= monster.health 0) (tset monster-record :dead true))
                 ))
      )
    )
  ;; (pp combat)
  
  )

logic
