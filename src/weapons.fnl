{
 ;;weapons warrior
 :short-sword-1
 {
  :name "Rusty Short Sword"
  :description "A lil rusty, but she'll get the job done"
  :icon :sword
  :type :weapon
  :rarity :common
  :level 1
  :params {:class [:warrior] :value 1}
  }

 :short-sword-2
 {
  :name "Short Sword"
  :description "Nothing special, but she'll at least try to cut"
  :icon :sword
  :type :weapon
  :rarity :common
  :level 2
  :params {:class [:warrior] :value 2}
  }
 
 :short-sword-3
 {
  :name "Improved Short Sword"
  :description "Now we're talking. This lil lady will cut through baked bread"
  :icon :sword
  :type :weapon
  :rarity :common
  :level 3
  :params {:class [:warrior] :value 3}
  }

 :long-sword-1
 {
  :name "Long Sword"
  :description "Nice and sharp and nice and long"
  :icon :sword
  :type :weapon
  :rarity :common
  :level 4
  :params {:class [:warrior] :value 4}
  }

  :long-sword-2
 {
  :name "Long Sword"
  :description "Almost too sharp for a gnome to handle"
  :icon :sword
  :type :weapon
  :rarity :common
  :level 5
  :params {:class [:warrior] :value 5}
  }

  :long-sword-3
 {
  :name "Long-er Sword"
  :description "This long sword is very long, perfect for lil gnomes"
  :icon :sword
  :type :weapon
  :rarity :common
  :level 6
  :params {:class [:warrior] :value 6}
  }

 
  :great-sword-1
 {
  :name "Great Sword"
  :description "While it's not that big, this sword sure is great!"
  :icon :sword
  :type :weapon
  :rarity :common
  :level 7
  :params {:class [:warrior] :value 7}
  }

  :great-sword-2
 {
  :name "Great Sword"
  :description "This sword will outdo any kitchen knife"
  :icon :sword
  :type :weapon
  :rarity :common
  :level 8
  :params {:class [:warrior] :value 7}
  }

 
  :great-sword-3
 {
  :name "Great-er Sword"
  :description "This great sword is very great, perfect for lil gnomes"
  :icon :sword
  :type :weapon
  :rarity :common
  :level 9
  :params {:class [:warrior] :value 9}
  }
 
 ;; Weapons Archer
 
 :short-bow-1
 {
  :name "Short Bow"
  :description "It won't shoot far, but it will shoot ... i think"
  :icon :bow
  :type :weapon
  :rarity :common
  :level 1
  :params {:class [:archer] :value 1}
  }

 :short-bow-2
 {
  :name "Short Bow"
  :description "Nothing special, but she'll draw and loose"
  :icon :bow
  :type :weapon
  :rarity :common
  :level 2
  :params {:class [:archer] :value 2}
  }
 
 :short-bow-3
 {
  :name "Improved Short Bow"
  :description "Now we're talking. You could shoot through a can of baked beans with this lil lady"
  :icon :bow
  :type :weapon
  :rarity :common
  :level 3
  :params {:class [:archer] :value 3}
  }


  :long-bow-1
 {
  :name "Long Bow"
  :description "Nice and sharp, this thing will shoot far"
  :icon :bow
  :type :weapon
  :rarity :common
  :level 4
  :params {:class [:archer] :value 4}
  }

 :long-bow-2
 {
  :name "Long Bow"
  :description "Can you hear the ringing? This bow is a ringer"
  :icon :bow
  :type :weapon
  :rarity :common
  :level 5
  :params {:class [:archer] :value 5}
  }
 
 :long-bow-3
 {
  :name "Long-er Bow"
  :description "This long bow can shoot further and harder than its counterparts"
  :icon :bow
  :type :weapon
  :rarity :common
  :level 6
  :params {:class [:archer] :value 6}
  }


  :great-bow-1
 {
  :name "Great Bow"
  :description "This bow is massive, almost too much for a gnome to handle."
  :icon :bow
  :type :weapon
  :rarity :common
  :level 7
  :params {:class [:archer] :value 8}
  }

 :great-bow-2
 {
  :name "Great Bow"
  :description "With this you could shoot from here to Iceville."
  :icon :bow
  :type :weapon
  :rarity :common
  :level 8
  :params {:class [:archer] :value 7}
  }
 
 :great-bow-3
 {
  :name "Great-er Bow"
  :description "This is the greatest bow. It can shoot further and harder than any other."
  :icon :bow
  :type :weapon
  :rarity :common
  :level 9
  :params {:class [:archer] :value 9}
  }
 
 ;; Weapons Mage

 :short-staff-1
 {
  :name "Rusty Short Staff"
  :description "A lil rusty, but she'll get the job done"
  :icon :staff
  :type :weapon
  :rarity :common
  :level 1
  :params {:class [:mage] :value 1}
  }

 :short-staff-2
 {
  :name "Short Staff"
  :description "Nothing special, but she's a staff"
  :icon :staff
  :type :weapon
  :rarity :common
  :level 2
  :params {:class [:mage] :value 2}
  }
 
 :short-staff-3
 {
  :name "Improved Short Staff"
  :description "Now we're talking. This lil lady will poke through baked bread"
  :icon :staff
  :type :weapon
  :rarity :common
  :level 3
  :params {:class [:mage] :value 3}
  }

 :long-staff-1
 {
  :name "Long Staff"
  :description "Nice and sharp and nice and long"
  :icon :staff
  :type :weapon
  :rarity :common
  :level 4
  :params {:class [:mage] :value 4}
  }

  :long-staff-2
 {
  :name "Long Staff"
  :description "Almost too sharp for a gnome to handle"
  :icon :staff
  :type :weapon
  :rarity :common
  :level 5
  :params {:class [:mage] :value 5}
  }

  :long-staff-3
 {
  :name "Long-er Staff"
  :description "This long staff is very long, perfect for lil gnomes"
  :icon :staff
  :type :weapon
  :rarity :common
  :level 6
  :params {:class [:mage] :value 6}
  }

 
  :great-staff-1
 {
  :name "Great Staff"
  :description "While it's not that big, this staff sure is great!"
  :icon :staff
  :type :weapon
  :rarity :common
  :level 7
  :params {:class [:mage] :value 7}
  }

  :great-staff-2
 {
  :name "Great Staff"
  :description "This staff will outdo any twig any day"
  :icon :staff
  :type :weapon
  :rarity :common
  :level 8
  :params {:class [:mage] :value 7}
  }

 
  :great-staff-3
 {
  :name "Great-er Staff"
  :description "This great staff is very great, perfect for lil gnomes"
  :icon :staff
  :type :weapon
  :rarity :common
  :level 9
  :params {:class [:mage] :value 9}
  }
 
}
