(local party (require :party))
(local tiles (require :tiles))

(local {: info} (require :state))



(fn missinform [by]
  (tset info :duke (+ info.duke by)))

(fn next-day []
  (local distance-modifier (-> (- 10 (tiles.party-distance-to-fortress)) (math.max 0) (/ 5)))
  (local movement-modifier (if (party.moved) 1 0.25))
  (local outdoor-modifier (if (party.outdoors) 2 1))
  (tset info :duke
        (-> info.base-rate (* outdoor-modifier distance-modifier movement-modifier) (+ info.duke)))
  (when (>= info.duke 200)
    (love.event.push :game-over :duke-kill)
    )
  )

{: next-day : missinform}
