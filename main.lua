-- bootstrap the compiler
fennel = require("lib.fennel")
table.insert(package.loaders, fennel.make_searcher({correlate=true}))

local make_love_searcher = function(env,predicate)
   return function(module_name)
      local path = predicate .. module_name:gsub("%.", "/") .. ".fnl"
      if love.filesystem.getInfo(path) then
         return function(...)
            local code = love.filesystem.read(path)
            return fennel.eval(code, {env=env}, ...)
         end, path
      end
   end
end

table.insert(package.loaders, make_love_searcher(_G, ""))
table.insert(package.loaders, make_love_searcher(_G, "src/"))
table.insert(fennel["macro-searchers"], make_love_searcher("_COMPILER", "src/"))

require ("globals")

math.randomseed( os.time() )

-- taken from bump
function pointWithin(px,py,x,y,w,h)
  return px < x + w  and px > x and
         py < y + h and py > y
end


require("wrap")
