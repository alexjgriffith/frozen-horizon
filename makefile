VERSION=0.2.2
LOVE_VERSION=11.4
NAME=frozen-horizon
ITCH_ACCOUNT=alexjgriffith
URL=https://alexjgriffith.itch.io/frozen-horizon
AUTHOR="AlexJGriffith"
DESCRIPTION="Your skyship has crashed on a frozen island."

##HEIGHT=$(shell grep t.window.height conf.lua | cut -d ' ' -f6)
##WIDTH=$(shell grep t.window.width conf.lua | cut -d ' ' -f6)

HEIGHT=700
WIDTH=1000
CANVAS_COLOUR="242,240,229"
TEXT_COLOUR="128,73,58"

# LIBS_LUA := $(wildcard lib/*)
# LIBS_FNL := $(wildcard lib/*.fnl)
LUA := $(wildcard *.lua)
SRC := $(wildcard src/*.fnl)
OUT := $(patsubst src/%.fnl,  %.fnl, $(SRC))
FNL := $(wildcard *.fnl)
LIBS_LUA := $(wildcard lib/*.lua)
LIBS := $(wildcard lib/*.fnl)

run: $(OUT) $(OUT_ENTS); love .

count: ; cloc src/*.fnl --force-lang=clojure

clean: ; rm -rf releases/* # $(OUT) $(OUT_LIBS)

LOVEFILE=releases/$(NAME)-$(VERSION).love

$(LOVEFILE): $(FNL) $(LUA) $(SRC) $(LIBS) $(LIBS_LUA) assets #text
	mkdir -p releases/
	find $^ -type f | LC_ALL=C sort | env TZ=UTC zip -r -q -9 -X $@ -@


love: $(LOVEFILE)

# platform-specific distributables

REL=$(PWD)/buildtools/love-release.sh # https://p.hagelb.org/love-release.sh
FLAGS=-a "$(AUTHOR)" --description $(DESCRIPTION) \
	--love $(LOVE_VERSION) --url $(URL) --version $(VERSION) --lovefile $(LOVEFILE)

releases/$(NAME)-$(VERSION)-x86_64.AppImage: $(LOVEFILE)
	cd buildtools/appimage && ./build.sh $(LOVE_VERSION) $(PWD)/$(LOVEFILE)
	mv buildtools/appimage/game-x86_64.AppImage $@

releases/$(NAME)-$(VERSION)-win.zip: $(LOVEFILE)
	$(REL) $(FLAGS) -W32
	mv releases/$(NAME)-win32.zip $@


releases/$(NAME)-$(VERSION)-web.zip: $(LOVEFILE)
	buildtools/love-js/love-js.sh releases/$(NAME)-$(VERSION).love $(NAME) -v=$(VERSION) -a=$(AUTHOR) -o=releases -w=$(WIDTH) -h=$(HEIGHT) -c=$(CANVAS_COLOUR) -t=$(TEXT_COLOUR)

releases/$(NAME)-$(VERSION)-source.zip: 
	find assets buildtools lib ./conf.lua ./makefile ./license.txt ./main.lua ./readme.org src buildtools -type f | LC_ALL=C sort | env TZ=UTC zip -r -q -9 -X $@ -@

runweb: $(LOVEFILE)
	echo $(HEIGHT) $(WIDTH)
	buildtools/love-js/love-js.sh releases/$(NAME)-$(VERSION).love $(NAME) -v=$(VERSION) -a=$(AUTHOR) -o=releases -w=$(WIDTH) -h=$(HEIGHT) -c=$(CANVAS_COLOUR) -t=$(TEXT_COLOUR) -r

myweb: $(LOVEFILE)
	HEIGHT=720
	WIDTH=1280
	CANVAS_COLOUR="78,88,74,1"
	TEXT_COLOUR="242,240,229"
	echo $(HEIGHT) $(WIDTH)
	buildtools/love-js/love-js.sh releases/$(NAME)-$(VERSION).love $(NAME) -v=$(VERSION) -a=$(AUTHOR)  -n -o=releases -w=$(WIDTH) -h=$(HEIGHT) -c=$(CANVAS_COLOUR) -t=$(TEXT_COLOUR)
	unzip releases/$(NAME)-$(VERSION)-web.zip
	rsync -ra  releases/$(NAME)-$(VERSION)-web/ "alexjgriffith:/home/griffita/nginx/resources/temp/frozen-horizon-720"


linux: releases/$(NAME)-$(VERSION)-x86_64.AppImage
windows: releases/$(NAME)-$(VERSION)-win.zip
web: releases/$(NAME)-$(VERSION)-web.zip
source: releases/$(NAME)-$(VERSION)-source.zip

# If you release on itch.io, you should install butler:
# https://itch.io/docs/butler/installing.html

uploadlinux: releases/$(NAME)-$(VERSION)-x86_64.AppImage
	butler push $^ $(ITCH_ACCOUNT)/$(NAME):linux --userversion $(VERSION)
uploadwindows: releases/$(NAME)-$(VERSION)-win.zip
	butler push $^ $(ITCH_ACCOUNT)/$(NAME):windows --userversion $(VERSION)
uploadlove: releases/$(NAME)-$(VERSION).love
	butler push $^ $(ITCH_ACCOUNT)/$(NAME):love --userversion $(VERSION)
uploadweb: releases/$(NAME)-$(VERSION)-web.zip
	butler push $^ $(ITCH_ACCOUNT)/$(NAME):web --userversion $(VERSION)
uploadsource: releases/$(NAME)-$(VERSION)-source.zip
	butler push $^ $(ITCH_ACCOUNT)/$(NAME):source --userversion $(VERSION)


upload: uploadlinux uploadwindows uploadweb uploadlove

release: linux web windows upload
