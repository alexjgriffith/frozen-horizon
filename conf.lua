p720 = false;

love.conf = function(t)
   t.title, t.identity = "frozen-horizon", "Frozen Horizon"
   t.modules.joystick = false
   t.modules.physics = false
   if (p720) then
      t.window.width = 1280 -- 1000 900
      t.window.height = 720 -- 700
      t.window.x = 0 -- windows bug
      t.window.y = 0 -- windows bug
   else
      t.window.width = 1000 -- 1000 900
      t.window.height = 700 -- 700
   end
   t.window.vsync = 1
   t.version = "11.4"
   t.window.icon = "assets/sprites/icon.png"
end
